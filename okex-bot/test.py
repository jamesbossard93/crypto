from src.enums.ccytype import CcyType
from src.api import account_api
import src.api.market_api as market
from src.api.trade_api import TradeAPI
import pandas as pd

import os
import toml
from src.market_data.market_data import market_data
from src.backtest.backtest import backtest_program, backtest_results
from src.backtest.backtest import plotting
from src.api.account_api import AccountAPI
from src.strategy.indicator_strategies import macd_strategy
from src.trade.trade import trade
import okex.Account_api as Account
import datetime as dt
import pytz
import time
# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #

config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #
api_key = config['API_CREDENTIALS']['api_key']

api_secret = config['API_CREDENTIALS']['api_secret']
api_passphrase = config['API_CREDENTIALS']['api_passphrase']
symbol = config['BOT_SETTINGS']['symbol']
market = market.MarketAPI(api_key=api_key, api_secret_key=api_secret, passphrase=api_passphrase)

""" tz = pytz.timezone("UTC")
orderbook = market.books(instId=symbol)
ask = orderbook[0]['asks'][0][0]
bid = orderbook[0]['bids'][0][0]
num_digits = max([ask, bid])
num_digits = num_digits[::-1].find('.')
tp = 10
sl = 30
bid = float(bid)
ask = float(ask)
tp = 10**(-num_digits)*tp
sl = 10**(-num_digits)*sl
tp = tp+float(ask)
tp = round(tp, num_digits)
sl = float(ask)-sl
sl = round(sl, num_digits)
print('tp: %s' % tp)
print('sl: %s' % sl) """

trade_client = TradeAPI(api_key=api_key, api_secret_key=api_secret, passphrase=api_passphrase)
result = trade_client.order(instId=symbol, tdMode='isolated', 
    sz=1, ordType='market', posSide='long')
#account = AccountAPI(api_key=api_key, api_secret_key=api_secret, passphrase=api_passphrase)
#account.positions()
#print(result)
#time.sleep(1)
order_details = trade_client.get_order(instId='XRP-USDT-SWAP')
account = AccountAPI(api_key=api_key, api_secret_key=api_secret, passphrase=api_passphrase)
pos = account.positions()[0]['upl']
print(pos)



