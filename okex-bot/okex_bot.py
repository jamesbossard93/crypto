from src.enums.ccytype import CcyType
from src.api import account_api
import src.api.market_api as market
from src.api.trade_api import TradeAPI

import pandas as pd
import okex.Trade_api as Trade

import os
import toml
from src.market_data.market_data import market_data
from src.backtest.backtest import backtest_program, backtest_results
from src.backtest.backtest import plotting
from src.strategy.indicator_strategies import macd_strategy
from src.strategy.indicator_strategies import bollinger_strategy
from src.trade.trade import trade
import okex.Account_api as Account

# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #

config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #


if config['BOT_SETTINGS']['backtesting'] is True:
    print('BACKTESTING!')
    print('----------------------------------------------------------------------------')
    if config['BOT_SETTINGS']['batch_backtesting'] is True:
        print("CURRENTLY LOOPING THROUGH 'symbol_list'...")
        backtest = backtest_program()

        results = backtest.run_batch_backtest()
        # do plotting here
        #plotting().plot_single()
        #print(results)
    else:
        symbol = config['BOT_SETTINGS']['symbol']
        # Plot description
        plot_desc = {}
        plot_desc['symbol'] = symbol
        plot_desc['strategy'] = "check_macd_with_stochrsi"

        tp = config['TRADE_SETTINGS']['take_profit']
        sl = config['TRADE_SETTINGS']['stop_loss']
        risk = config['TRADE_SETTINGS']['risk_percentage']
        leverage = config['TRADE_SETTINGS']['leverage']
        api_key = config['API_CREDENTIALS']['api_key']
        api_secret = config['API_CREDENTIALS']['api_secret']
        api_passphrase = config['API_CREDENTIALS']['api_passphrase']
        accountAPI = Account.AccountAPI(api_key, api_secret, api_passphrase, False, '0')
        esl = config['BOT_SETTINGS']['run_esl']
        #print(accountAPI)
        #print(accountAPI.get_account())
        balance = accountAPI.get_account()['data'][0]['details'][0]['availEq']
        balance = float(balance)
        balance = round(balance, 4)
        margin = risk * balance
        margin = round(margin, 2)
        plot_desc['margin'] = str(margin)
        plot_desc['strategy'] = "simple_bollinger_2"
        print("RUNNING BACKTEST FOR SYMBOL: %s" % symbol)
        backtest = backtest_program()
        df = market_data().get_kline_bars(symbol)
        #df = df*1000
        #print(df.head())
        #print(df)
        #print(df.tail())
        #indicator = macd_strategy().check_macd_with_stochrsi(df)
        #indicator = bollinger_strategy().stochrsi_macd_bollinger_indicator(df)
        #indicator = bollinger_strategy().stochrsi_macd_bollinger_indicatorv2(df)
        #indicator = bollinger_strategy().simple_bollinger_1(df)
        indicator = bollinger_strategy().simple_bollinger_2(df)    
        #df = df/1000
        #print(indicator)
        #indicator = macd_strategy().check_macd_with_stochrsiv2(df)
        if esl is True:
            results, pl_results = backtest.with_esl(df, indicator, tp, sl, margin, leverage, 2, 20)
        else:
            results, pl_results = backtest.no_esl(df, indicator, tp, sl, margin, leverage)
            #print(results)
            #print(pl_results)
            #results, pl_results = backtest.no_esl_v2(df, indicator, tp, sl, margin, leverage)

        #print(pl_results)
        description = backtest_results().describe_results(results)
        profits = backtest_results().calculate_profits(tp, sl, description=description)
        plotting().plot_single(pl_results, description, plot_desc)
        print(description)
        print(profits)
else:
    symbol = config['BOT_SETTINGS']['symbol']
    print('RUNNING LIVE BOT!')
    trade().run_trade(symbol)