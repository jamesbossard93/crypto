import math
from warnings import resetwarnings
import toml
import os
import datetime as dt
import pandas as pd
import time
from ta.volume import volume_weighted_average_price

# TA library imports
from ta.trend import ema_indicator
from ta.trend import macd_diff
from ta.momentum import StochRSIIndicator
from ta.volatility import BollingerBands

# Project Imports
from src.market_data.market_data import market_data
from src.okex_py import helper_functions

# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #

config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #



class macd_strategy:
    def __init__(self):
        pass

    # ---------------------------------------------------------------------------- #
    #                      MACD HISTOGRAM WITH STOCHASTIC RSI                      #
    # ---------------------------------------------------------------------------- #

    def check_macd_with_stochrsi(self, df):
        # data = pandas dataframe of all indicators including close price
        # window = range/bars to look
        # rsi_delta = the error term when looking at the rsi levels.
        # between close price and ema
        # checks for stochastic crossover while above 0.8 or below 0.2
        close_data = df['close']
        # Steps:
        # 1. Check if ema is uptrend or downtrend.
        # 2. if so, look for when stochrsi <= 0.2 + rsi_delta
        # 3. confirm move with MACD histogram (diff).
        macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
        ema = ema_indicator(close_data, window=50, fillna=False)
        # Use stoch_rsi_d to search for divergences and find entry points
        stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
            smooth1=1, smooth2=1, fillna=False).stochrsi_d()
        stoch_rsi_d = stoch_rsi_d.dropna()

        #print(stoch_rsi_d)
        #ema = pd.DataFrame(ema)
        min_list = min([len(stoch_rsi_d), len(ema), len(macd_histogram), len(close_data)])
        # ---------------------------------------------------------------------------------------------------------
        close_data = close_data.tail(min_list)
        macd_histogram = macd_histogram.tail(min_list)
        ema = ema.tail(min_list)
        stoch_rsi_d = stoch_rsi_d.tail(min_list)

        window_list = []

        for i in range(min_list):

            #first check if ema is uptrend or downtrend
            if math.exp(ema[i]-ema[i-1]) < 1:
                # if ema is increasing
                if stoch_rsi_d[i] >= 0.6:
                    if macd_histogram[i] >= 0.05:
                        window_list.append("SELL")
                    else:
                        window_list.append("IGNORE")
                elif stoch_rsi_d[i] <= 0.4:
                    if macd_histogram[i] <= -0.05:
                        window_list.append("BUY")
                    else:
                        window_list.append("IGNORE")

                else:
                    window_list.append("IGNORE")

            elif math.exp(ema[i]-ema[i-1]) > 1:
                # if ema is decreasing
                if stoch_rsi_d[i] >= 0.6:
                    if macd_histogram[i] >= 0.05:
                        window_list.append("SELL")
                    else:
                        window_list.append("IGNORE")
                elif stoch_rsi_d[i] <= 0.4:
                    if macd_histogram[i] <= -0.05:
                        window_list.append("BUY")
                    else:
                        window_list.append("IGNORE")

                else:
                    window_list.append("IGNORE")
            else:
                window_list.append("IGNORE")

        return window_list


    # ---------------------------------------------------------------------------- #
    #                      MACD HISTOGRAM WITH STOCHASTIC RSI                      #
    # ---------------------------------------------------------------------------- #

    def check_macd_with_stochrsiv2(self, df):
        # data = pandas dataframe of all indicators including close price
        # window = range/bars to look
        # rsi_delta = the error term when looking at the rsi levels.
        # between close price and ema
        # checks for stochastic crossover while above 0.8 or below 0.2
        close_data = df['close']
        # Steps:
        # 1. Check if ema is uptrend or downtrend.
        # 2. if so, look for when stochrsi <= 0.2 + rsi_delta
        # 3. confirm move with MACD histogram (diff).
        macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
        ema = ema_indicator(close_data, window=50, fillna=False)
        trend = self.trend.ema_trend(20)
        # Use stoch_rsi_d to search for divergences and find entry points
        stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
            smooth1=1, smooth2=1, fillna=False).stochrsi_d()
        stoch_rsi_d = stoch_rsi_d.dropna()

        #print(stoch_rsi_d)
        #ema = pd.DataFrame(ema)
        min_list = min([len(stoch_rsi_d), len(trend), len(macd_histogram), len(close_data)])
        # ---------------------------------------------------------------------------------------------------------
        close_data = close_data.tail(min_list)
        macd_histogram = macd_histogram.tail(min_list)
        trend = trend[-min_list:]
        stoch_rsi_d = stoch_rsi_d.tail(min_list)

        window_list = []

        for i in range(min_list):

            #first check if ema is uptrend or downtrend
            if trend[i] == "UPTREND":
                # if ema is increasing
                if stoch_rsi_d[i] >= 0.6:
                    if macd_histogram[i] >= 0.05:
                        window_list.append("SELL")
                    else:
                        window_list.append("IGNORE")
                elif stoch_rsi_d[i] <= 0.4:
                    if macd_histogram[i] <= -0.05:
                        window_list.append("BUY")
                    else:
                        window_list.append("IGNORE")

                else:
                    window_list.append("IGNORE")

            elif trend[i] == "DOWNTREND":
                # if ema is decreasing
                if stoch_rsi_d[i] >= 0.6:
                    if macd_histogram[i] >= 0.05:
                        window_list.append("SELL")
                    else:
                        window_list.append("IGNORE")
                elif stoch_rsi_d[i] <= 0.4:
                    if macd_histogram[i] <= -0.05:
                        window_list.append("BUY")
                    else:
                        window_list.append("IGNORE")

                else:
                    window_list.append("IGNORE")
            else:
                window_list.append("IGNORE")

        return window_list


class bollinger_strategy:
    def __init__(self):
        #self.df = market_data.get_kline_bars()
        self.trend = helper_functions.trends()
    # ---------------------------------------------------------------------------- #
    #       TRIPLE THREAT STRATEGY: STOCH_RSI+MACD_HISTOGRAM+BOLLINGER_BANDS       #
    # ---------------------------------------------------------------------------- #

    def stochrsi_macd_bollinger_indicator(self, df, b_band_delta=0.001):
        ''' Requires bollinger band delta as input.'''
        # THIS STRATEGY IS A CONTINUATION TREND STRATEGY.
        # IT DOES NOT TRY TO 'CHASE' THE TOPS AND BOTTOMS LIKE
        # MOST STRATEGIES.
        close_data = df['close']
        #close_data = close_data.iloc[200:]

        # trend analysis
        trend = self.trend.ema_trend(df, 50)
        macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
        stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
            smooth1=1, smooth2=1, fillna=False).stochrsi_d()
        stoch_rsi_d = stoch_rsi_d.dropna()
        hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
        lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
        mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
        min_list = min([len(stoch_rsi_d), len(hband), len(macd_histogram), len(close_data), len(trend)])

        close_data = close_data.tail(min_list).to_list()
        macd_histogram = macd_histogram.tail(min_list).to_list()
        stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()
        hband = hband.tail(min_list).to_list()
        lband = lband.tail(min_list).to_list()
        mband = mband.tail(min_list).to_list()
        trend = pd.Series(trend).tail(min_list).to_list()



        test_list = []
        results_list = []

        for i in range(min_list):
            #current_tick_time = df_times[i]
            #from_time = current_tick_time-timedelta(hours=3)
            #trend =
            #from_time = current_tick_time-timedelta(days=2)
            #time_now = datetime.now(timezone)+daylight_savings

            # first do something
            delta_lband = abs(lband[i] - close_data[i])
            delta_hband = abs(hband[i] - close_data[i])
            delta_mband = abs(mband[i] - close_data[i])

            if trend[i] == "UPTREND":
                # check for pullback during uptrend
                if delta_mband <= b_band_delta:
                    if 0 < macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5:
                        # pullback BUY
                        results_list.append("BUY")
                    else:
                        results_list.append("IGNORE")
                elif lband[i] < close_data[i] < mband[i]:
                    if macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5: 
                        # pullback BUY
                        results_list.append("BUY")
                    else:
                        results_list.append("IGNORE")
                else:
                    results_list.append("IGNORE")

            elif trend[i] == "DOWNTREND":
                # check for potential sells during pullback
                asdf = delta_mband
                if delta_mband <= b_band_delta:
                    if -0.2 < macd_histogram[i] < 0 and 0.3 <= stoch_rsi_d[i]:
                        # pullback SELL
                        results_list.append("SELL")
                    else:
                        results_list.append("IGNORE")
                else:
                    results_list.append("IGNORE")
            else:
                # SIDEWAYS MARKET
                # check macd and stochrsi here!
                
                if delta_hband <= b_band_delta:
                    # check macd and stochrsi for sell
                    if macd_histogram[i] >= 0 and stoch_rsi_d[i] > 0.5:
                        results_list.append("SELL")
                    else:
                        results_list.append("IGNORE")
                elif delta_lband <= b_band_delta:
                    if macd_histogram[i] <= 0 and stoch_rsi_d[i] < 0.5:
                        results_list.append("BUY")
                    else:
                        results_list.append("IGNORE")
        # returns a list with the oldest data first.
        return results_list

    # ---------------------------------------------------------------------------- #
    #       TRIPLE THREAT STRATEGY v2: STOCH_RSI+MACD_HISTOGRAM+BOLLINGER_BANDS    #
    # ---------------------------------------------------------------------------- #

    def stochrsi_macd_bollinger_indicatorv2(self, df, b_band_delta=0.0025):
        ''' Requires bollinger band delta as input.'''
        # THIS STRATEGY IS A CONTINUATION TREND STRATEGY.
        # IT DOES NOT TRY TO 'CHASE' THE TOPS AND BOTTOMS LIKE
        # MOST STRATEGIES.
        close_data = df['close']
        #close_data = close_data.iloc[200:]

        # trend analysis
        trend = self.trend.ema_trend(df, 50)
        macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
        stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
            smooth1=1, smooth2=1, fillna=False).stochrsi_d()
        stoch_rsi_d = stoch_rsi_d.dropna()
        hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
        lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
        mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
        min_list = min([len(stoch_rsi_d), len(hband), len(macd_histogram), len(close_data), len(trend)])

        close_data = close_data.tail(min_list).to_list()
        macd_histogram = macd_histogram.tail(min_list).to_list()
        stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()
        hband = hband.tail(min_list).to_list()
        lband = lband.tail(min_list).to_list()
        mband = mband.tail(min_list).to_list()
        trend = pd.Series(trend).tail(min_list).to_list()


        results_list = []

        for i in range(min_list):
            #current_tick_time = df_times[i]
            #from_time = current_tick_time-timedelta(hours=3)
            #trend =
            #from_time = current_tick_time-timedelta(days=2)
            #time_now = datetime.now(timezone)+daylight_savings

            # first do something
            delta_lband = abs(lband[i] - close_data[i])
            delta_hband = abs(hband[i] - close_data[i])
            delta_mband = abs(mband[i] - close_data[i])

            if trend[i] == "UPTREND":
                # check for pullback during uptrend
                if delta_lband <= delta_mband:
                    if 0 < macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5:
                        # pullback BUY
                        results_list.append("BUY")
                    else:
                        results_list.append("IGNORE")
                else:
                    results_list.append("IGNORE")

            elif trend[i] == "DOWNTREND":
                # check for potential sells during pullback
                asdf = delta_mband
                if delta_hband <= delta_mband:
                    if -0.2 < macd_histogram[i] < 0 and 0.3 <= stoch_rsi_d[i]:
                        # pullback SELL
                        results_list.append("SELL")
                    else:
                        results_list.append("IGNORE")
                else:
                    results_list.append("IGNORE")
            else:
                # SIDEWAYS MARKET
                # check macd and stochrsi here!
                
                if delta_hband <= delta_mband:
                    # check macd and stochrsi for sell
                    if macd_histogram[i] >= 0 and stoch_rsi_d[i] > 0.5:
                        results_list.append("SELL")
                    else:
                        results_list.append("IGNORE")
                elif delta_lband <= delta_mband:
                    if macd_histogram[i] <= 0 and stoch_rsi_d[i] < 0.5:
                        results_list.append("BUY")
                    else:
                        results_list.append("IGNORE")
        # returns a list with the oldest data first.
        return results_list

    def simple_bollinger_1(self, df, b_band_delta=0.05):

        close_data = df['close']
        high_data = df['high']
        low_data = df['low']
        #close_data = close_data.iloc[200:]

        # trend analysis
        trend = self.trend.ema_trend(df, 50)
        hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
        lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
        mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
        list_of_lists = [len(hband), len(close_data), len(trend)]
        #print(list_of_lists)
        min_list = min(list_of_lists)
        #print(min_list)
        close_data = close_data.tail(min_list).to_list()
        high_data = high_data.tail(min_list).to_list()
        low_data = low_data.tail(min_list).to_list()
        hband = hband.tail(min_list).to_list()
        lband = lband.tail(min_list).to_list()
        mband = mband.tail(min_list).to_list()
        trend = pd.Series(trend).tail(min_list).to_list()
        #print(len(close_data))
        #print(len(hband))
        
        #print(len(trend))
        results = []
        for i in range(min_list):
            delta_lband = abs(lband[i] - close_data[i])
            delta_hband = abs(hband[i] - close_data[i])
            delta_mband = abs(mband[i] - close_data[i])
            if trend[i] == 'UPTREND':
                if delta_lband < delta_mband and delta_lband < b_band_delta:
                    results.append("BUY")
                elif delta_mband < delta_hband and delta_mband < b_band_delta:
                    results.append("BUY")
                else:
                    results.append("IGNORE")

            elif trend[i] == 'DOWNTREND':
                if delta_hband < delta_mband and delta_hband < b_band_delta or hband[i] < high_data[i]:
                    results.append("SELL")
                elif delta_mband < delta_lband and delta_mband < b_band_delta and close_data[i] < mband[i]:
                    results.append("SELL")
                else:
                    results.append("IGNORE")

            elif trend[i] == 'SIDEWAYS':
                if delta_hband < delta_mband and delta_hband < b_band_delta or hband[i] < high_data[i]:
                    results.append("SELL")
                elif delta_lband < delta_mband and delta_lband < b_band_delta or low_data[i] < lband[i]:
                    results.append("BUY")
                else:
                    results.append("IGNORE")
            else:
                results.append("IGNORE")

        return results
                

    def simple_bollinger_2(self, df, b_band_delta=0.08):

        close_data = df['close']
        low_data = df['low']
        high_data = df['high']
        #close_data = close_data.iloc[200:]

        # trend analysis
        trend = self.trend.ema_trend(df, 50)
        hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
        lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
        mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
        list_of_lists = [len(hband), len(close_data), len(trend)]
        #print(list_of_lists)
        min_list = min(list_of_lists)
        #print(min_list)
        close_data = close_data.tail(min_list).to_list()
        low_data = low_data.tail(min_list).to_list()
        high_data = high_data.tail(min_list).to_list()
        hband = hband.tail(min_list).to_list()
        lband = lband.tail(min_list).to_list()
        mband = mband.tail(min_list).to_list()
        trend = pd.Series(trend).tail(min_list).to_list()
        #print(len(close_data))
        #print(len(hband))
        
        #print(len(trend))
        results = []
        for i in range(min_list):
            delta_lband_low = abs(lband[i] - low_data[i])
            delta_lband = abs(lband[i] - close_data[i])

            delta_hband = abs(hband[i] - close_data[i])
            delta_hband_high = abs(hband[i] - high_data[i])
            delta_mband = abs(mband[i] - close_data[i])

            if delta_lband < delta_mband and delta_lband_low < b_band_delta or low_data[i] < lband[i]:
                results.append("BUY")
            elif delta_hband < delta_mband and delta_hband_high < b_band_delta or high_data[i] > hband[i]:
                    results.append("SELL")
            else:
                results.append("IGNORE")

        return results

    # ---------------------------------------------------------------------------- #
    #                          DOUBLE VWAP BOLLINGER BANDS                         #
    # ---------------------------------------------------------------------------- #

    def vwap_bollinger_1(self, df, b_band_delta=0.08):
        # optional vwap windows; 1440, 720, 360
        vwap_band = volume_weighted_average_price(df['high'], df['low'], 
            df['close'], df['volume'], window=720, fillna=True)
        vwap_band_upper_1 = vwap_band+vwap_band.std()
        vwap_band_lower_1 = vwap_band-vwap_band.std()
        vwap_band_upper_2 = vwap_band+vwap_band.std()*2
        vwap_band_lower_2 = vwap_band-vwap_band.std()*2

        close_data = df['close']
        high_data = df['high']
        low_data = df['low']
        #close_data = close_data.iloc[200:]

        # trend analysis
        trend = self.trend.ema_trend(df, 50)
        hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
        lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
        mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
        list_of_lists = [len(hband), len(close_data), len(trend), len(vwap_band), len(vwap_band_lower_1)]
        #print(list_of_lists)
        min_list = min(list_of_lists)
        #print(min_list)
        close_data = close_data.tail(min_list).to_list()
        low_data = low_data.tail(min_list).to_list()
        high_data = high_data.tail(min_list).to_list()


        hband = hband.tail(min_list).to_list()
        lband = lband.tail(min_list).to_list()
        mband = mband.tail(min_list).to_list()
        #trend = pd.Series(trend).tail(min_list).to_list()
        vwap_band = vwap_band.tail(min_list).to_list()
        vwap_band_lower_1 = vwap_band_lower_1.tail(min_list).to_list()
        vwap_band_upper_1 = vwap_band_upper_1.tail(min_list).to_list()
        vwap_band_lower_2 = vwap_band_lower_2.tail(min_list).to_list()
        vwap_band_upper_2 = vwap_band_upper_2.tail(min_list).to_list()
        
        df = df[['open', 'high', 'low', 'close']]

        df = df.tail(min_list)
        #print(len(vwap_band))
        #print(vwap_band)
        #print(len(hband))
        #print(len(close_data))
        df['hband'] = hband
        df['lband'] = lband
        df['mband'] = mband
        df['vwap_band'] = vwap_band
        df['vwap_band_upper_1'] = vwap_band_upper_1
        df['vwap_band_lower_1'] = vwap_band_lower_1
        df['vwap_band_upper_2'] = vwap_band_upper_2
        df['vwap_band_lower_2'] = vwap_band_lower_2
        # if delta_vwap_band_upper_2
        
        #delta_lband = abs(lband[idx] - close_data[idx])
        #delta_hband = abs(hband[idx] - close_data[idx])
        #delta_mband = abs(mband[idx] - close_data[idx])
        #delta_vwap = abs(vwap_band[idx] - )
        #delta_vwap_band_lower_2 > low_data[idx] or abs(delta_vwap_band_lower_2-)

        #return df


        results = []
        for idx in range(min_list):
            # Bollinger diff
            delta_lband = abs(lband[idx] - close_data[idx])
            delta_lband_low = abs(lband[idx] - low_data[idx])
            delta_hband = abs(hband[idx] - close_data[idx])
            delta_hband_high = abs(hband[idx] - high_data[idx])
            delta_mband = abs(mband[idx] - close_data[idx])
            # VWAP diff
            delta_vwap = abs(vwap_band[idx] - close_data[idx])
            delta_vwap_band_upper_1 = abs(vwap_band_upper_1[idx] - high_data[idx])
            delta_vwap_band_upper_2 = abs(vwap_band_upper_2[idx] - high_data[idx])
            delta_vwap_band_lower_1 = abs(vwap_band_lower_1[idx] - low_data[idx])
            delta_vwap_band_lower_2 = abs(vwap_band_lower_2[idx] - low_data[idx])

            if low_data[idx] < vwap_band_lower_2[idx] or delta_vwap_band_lower_2 < b_band_delta:
                if delta_lband_low < b_band_delta or low_data[idx] < lband[idx]:
                    results.append('BUY')
                else:
                    results.append('IGNORE')
            elif vwap_band_lower_2[idx] < low_data[idx] < vwap_band_lower_1[idx]:
                if delta_lband_low < b_band_delta or low_data[idx] < lband[idx]:
                    results.append('BUY')
                else:
                    results.append('IGNORE')

            elif high_data[idx] > vwap_band_upper_2[idx] or delta_vwap_band_upper_2 < b_band_delta:
                if delta_hband_high < b_band_delta or hband[idx] < high_data[idx]:
                    results.append('SELL')
                else:
                    results.append('IGNORE')

            elif vwap_band_upper_1[idx] < high_data[idx] < vwap_band_upper_2[idx]:
                if delta_hband_high < b_band_delta or hband[idx] < high_data[idx]:
                    results.append('SELL')
                else:
                    results.append('IGNORE')

            else:
                results.append('IGNORE')

        return results



    # ---------------------------------------------------------------------------- #
    #                          DOUBLE VWAP BOLLINGER BANDS                         #
    # ---------------------------------------------------------------------------- #

    def vwap_bollinger_1(self, df, b_band_delta=0.08):
        # optional vwap windows; 1440, 720, 360
        vwap_band = volume_weighted_average_price(df['high'], df['low'], 
            df['close'], df['volume'], window=720, fillna=True)
        vwap_band_upper_1 = vwap_band+vwap_band.std()
        vwap_band_lower_1 = vwap_band-vwap_band.std()
        vwap_band_upper_2 = vwap_band+vwap_band.std()*2
        vwap_band_lower_2 = vwap_band-vwap_band.std()*2

        close_data = df['close']
        high_data = df['high']
        low_data = df['low']
        #close_data = close_data.iloc[200:]

        # trend analysis
        trend = self.trend.ema_trend(df, 50)
        hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
        lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
        mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
        list_of_lists = [len(hband), len(close_data), len(trend), len(vwap_band), len(vwap_band_lower_1)]
        #print(list_of_lists)
        min_list = min(list_of_lists)
        #print(min_list)
        close_data = close_data.tail(min_list).to_list()
        low_data = low_data.tail(min_list).to_list()
        high_data = high_data.tail(min_list).to_list()


        hband = hband.tail(min_list).to_list()
        lband = lband.tail(min_list).to_list()
        mband = mband.tail(min_list).to_list()
        #trend = pd.Series(trend).tail(min_list).to_list()
        vwap_band = vwap_band.tail(min_list).to_list()
        vwap_band_lower_1 = vwap_band_lower_1.tail(min_list).to_list()
        vwap_band_upper_1 = vwap_band_upper_1.tail(min_list).to_list()
        vwap_band_lower_2 = vwap_band_lower_2.tail(min_list).to_list()
        vwap_band_upper_2 = vwap_band_upper_2.tail(min_list).to_list()
        
        df = df[['open', 'high', 'low', 'close']]

        df = df.tail(min_list)
        #print(len(vwap_band))
        #print(vwap_band)
        #print(len(hband))
        #print(len(close_data))
        df['hband'] = hband
        df['lband'] = lband
        df['mband'] = mband
        df['vwap_band'] = vwap_band
        df['vwap_band_upper_1'] = vwap_band_upper_1
        df['vwap_band_lower_1'] = vwap_band_lower_1
        df['vwap_band_upper_2'] = vwap_band_upper_2
        df['vwap_band_lower_2'] = vwap_band_lower_2
        # if delta_vwap_band_upper_2
            
        #delta_lband = abs(lband[idx] - close_data[idx])
        #delta_hband = abs(hband[idx] - close_data[idx])
        #delta_mband = abs(mband[idx] - close_data[idx])
        #delta_vwap = abs(vwap_band[idx] - )
        #delta_vwap_band_lower_2 > low_data[idx] or abs(delta_vwap_band_lower_2-)

        #return df


        results = []
        for idx in range(min_list):
            # Bollinger diff
            delta_lband = abs(lband[idx] - close_data[idx])
            delta_lband_low = abs(lband[idx] - low_data[idx])
            delta_hband = abs(hband[idx] - close_data[idx])
            delta_hband_high = abs(hband[idx] - high_data[idx])
            delta_mband = abs(mband[idx] - close_data[idx])
            # VWAP diff
            delta_vwap = abs(vwap_band[idx] - close_data[idx])
            delta_vwap_band_upper_1 = abs(vwap_band_upper_1[idx] - high_data[idx])
            delta_vwap_band_upper_2 = abs(vwap_band_upper_2[idx] - high_data[idx])
            delta_vwap_band_lower_1 = abs(vwap_band_lower_1[idx] - low_data[idx])
            delta_vwap_band_lower_2 = abs(vwap_band_lower_2[idx] - low_data[idx])

            #if vwap_band_lower_2[idx] < close_data[idx] < vwap_band_lower_1[idx]:
            #    if low_data[idx] < lband[idx]:
            #        results.append("BUY")
            #    elif 