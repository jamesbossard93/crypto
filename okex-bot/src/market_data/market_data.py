import okex.Market_api as Market
import pytz
import toml
import os
import datetime as dt
import pandas as pd
import time
import numpy as np
from src.api.account_api import AccountAPI
from src.api.trade_api import TradeAPI
# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #


config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #
# flag是实盘与模拟盘的切换参数 flag is the key parameter which can help you to change between demo and real trading.
# flag = '1'  # 模拟盘 demo trading
# flag = '0'  # 实盘 real trading

class market_data:
    def __init__(self):
        self.market_type = config['BOT_SETTINGS']['market_type']
        self.symbol = config['BOT_SETTINGS']['symbol']
        self.symbol_list = config['BOT_SETTINGS']['symbol_list']
        self.interval = config['BOT_SETTINGS']['interval']
        self.num_batches = config['BOT_SETTINGS']['num_batches']
        self.api_key = config['API_CREDENTIALS']['api_key']
        self.api_secret = config['API_CREDENTIALS']['api_secret']
        self.api_passphrase = config['API_CREDENTIALS']['api_passphrase']
        self.marketAPI = Market.MarketAPI(self.api_key, self.api_secret, self.api_passphrase, False, '0')
        self.account = AccountAPI(self.api_key, self.api_secret, self.api_passphrase)
        self.trade_client = TradeAPI(self.api_key, self.api_secret, self.api_passphrase)
        self.skip_amount = config['BOT_SETTINGS']['skip_amount']
        self.tp = config['TRADE_SETTINGS']['take_profit']
        self.sl = config['TRADE_SETTINGS']['stop_loss']

    def get_all_ticker_info(self):
        result = self.marketAPI.get_tickers("SWAP")
        """ ['instType' 'instId' 'last' 'lastSz' 'askPx' 'askSz' 'bidPx' 'bidSz'
        'open24h' 'high24h' 'low24h' 'volCcy24h' 'vol24h' 'ts' 'sodUtc0'
        'sodUtc8'] """
        df = pd.DataFrame(result)
        df = df[['instId', 'last', 'open24h', 'high24h', 'low24h', 'vol24h']]
        df = df.rename(columns={"instId": "Symbol"})

        # returns only last, ohlv
        return df

    def get_order_book(self, symbol, bidask):
        if bidask == 'ask':
            result = self.marketAPI.get_orderbook(symbol, '400')['data'][0]['asks']
            df = pd.DataFrame(result, columns=['price', 'amount', '0', 'total'])
            df = df[['price', 'amount']]
        elif bidask == 'bid':
            result = self.marketAPI.get_orderbook(symbol, '400')['data'][0]['bids']
            df = pd.DataFrame(result, columns=['price', 'amount', '0', 'total'])
            df = df[['price', 'amount']]

        return df

    def get_kline_bars(self, symbol):
        tz = pytz.timezone("UTC")
        df_list = []
        # in datetime format
        num_batches = self.num_batches
        i = 0
        while i<num_batches:
            batch = (i*100)#+100
            batch_2 = batch+100
            from_time = dt.datetime.now(tz)-dt.timedelta(minutes=batch)
            to_time = from_time-dt.timedelta(minutes=batch_2)
            from_time = round(dt.datetime.timestamp(from_time)*1000)
            to_time = round(dt.datetime.timestamp(to_time)*1000)
            result = self.marketAPI.get_candlesticks(symbol,after=from_time, before=to_time, bar='1m', limit=100)['data']
            #result = self.marketAPI.get_history_candlesticks(symbol,after=from_time, before=to_time, bar='1m', limit=100)['data']
            df = pd.DataFrame(result, columns=['time', 'open', 'high', 'low', 'close', 'vol', 'volume'])
            df = df[['time', 'open', 'high', 'low', 'close', 'volume']]
            if df.empty:
                break
            df_list.append(df)
            i+=1
            time.sleep(0.1)

        df2 = pd.concat(df_list)
        df_time = df2['time'].to_numpy()
        df_time = df_time.astype(float)
        df_time = df_time/1000
        df_time = np.round(df_time, decimals=0)
        df_time = df_time.astype(int)
        dftime = []
        for i in range(len(df_time)):
            dftime.append(dt.datetime.fromtimestamp(df_time[i], tz).strftime("%Y-%m-%d %H:%M"))
        x = pd.Series(dftime)
        df2['time'] = x.to_list()
        df2 = df2.iloc[::-1]
        df2.set_index('time', drop=True, inplace=True)
        df2['open'] = pd.to_numeric(df2['open'], downcast="float")
        df2['high'] = pd.to_numeric(df2['high'], downcast="float")
        df2['low'] = pd.to_numeric(df2['low'], downcast="float")
        df2['close'] = pd.to_numeric(df2['close'], downcast="float")
        df2['volume'] = pd.to_numeric(df2['volume'], downcast="float")

        return df2

    
    def get_multiple_kline_bars(self):
        df_dict = {}

        symbol_list = self.symbol_list

        for i in symbol_list:
            print("Acquiring data for %s..." % i)
            df = self.get_kline_bars(i)
            df_dict[i] = df
        
        return df_dict

    def get_historical_trades(self, symbol):
        result = self.trade_client.get_order_history(instType="SWAP", instId=symbol)
        df = pd.DataFrame(result)
        df = df[['cTime','side', 'pnl']]
        df["pnl"] = pd.to_numeric(df["pnl"], downcast="float")
        df = df[df.pnl != 0]
        df = df.rename(columns={"cTime": "time"})
        df.set_index('time', inplace=True, drop=False)
        return df

    def get_esl_time(self, symbol):
        historical_trades = self.get_historical_trades(symbol)
        esl_time = historical_trades.index.values[-1]

        return esl_time

    def check_position(self, symbol, tp, sl):
        tp = tp/(len(str(tp))+1)
        sl = sl/(len(str(sl))+1)
        df = self.account.positions()
        df = pd.DataFrame(df)
        if df.empty or df is None:
            return None
        pnl = self.account.positions()[0]['upl']
        pnl = float(pnl)
        side = self.account.positions()[0]['posSide']
        closed_order = None
        print('Current pnl: %s' % pnl)
        if pnl >= 0.3:
            # close position
            closed_order = self.trade_client.close_positions(instId=symbol, 
                posSide=side, mgnMode='isolated')
            print("CLOSING LONG POSITION")
            print(closed_order)
            if closed_order != None:
                # do while loop here!
                while closed_order != None:
                    closed_order = self.check_position(symbol, self.tp, self.sl)         
                    time.sleep(1)
                    print("MONITORING OPEN POSITION...")
                else:
                    print("CLOSED POSITION")
                    print('EMERGENCY STOP LOSS ACTIVE')
                    esl_counter = 0

                    while esl_counter < self.skip_amount:
                        print('EMERGENCY STOP LOSS ACTIVE')
                        time.sleep(60)
                        esl_counter+=1
                        if esl_counter == self.skip_amount:
                            esl_counter = -1
                            esl_bool = False
                            break

        elif pnl <= -0.10:
            # close position
            closed_order = self.trade_client.close_positions(instId=symbol, 
                posSide=side, mgnMode='isolated')
            print("CLOSING SHORT POSITION")
            print(closed_order)
            if closed_order != None:
                # do while loop here!
                while closed_order != None:
                    closed_order = self.check_position(symbol, self.tp, self.sl)         
                    time.sleep(1)
                    print("MONITORING OPEN POSITION...")
                else:
                    print("CLOSED POSITION")
                    print('EMERGENCY STOP LOSS ACTIVE')
                    esl_counter = 0

                    while esl_counter < self.skip_amount:
                        print('EMERGENCY STOP LOSS ACTIVE')
                        time.sleep(60)
                        esl_counter+=1
                        if esl_counter == self.skip_amount:
                            esl_counter = -1
                            esl_bool = False
                            break
        else:
            return closed_order


    def get_kline_bars_trade(self, symbol, tp, sl):
        tz = pytz.timezone("UTC")
        df_list = []
        # in datetime format
        num_batches = self.num_batches
        i = 0
        while i<num_batches:
            batch = (i*100)#+100
            batch_2 = batch+100
            from_time = dt.datetime.now(tz)-dt.timedelta(minutes=batch)
            to_time = from_time-dt.timedelta(minutes=batch_2)
            from_time = round(dt.datetime.timestamp(from_time)*1000)
            to_time = round(dt.datetime.timestamp(to_time)*1000)
            result = self.marketAPI.get_candlesticks(symbol,after=from_time, before=to_time, bar='1m', limit=100)['data']
            df = pd.DataFrame(result, columns=['time', 'open', 'high', 'low', 'close', 'vol', 'volume'])
            df = df[['time', 'open', 'high', 'low', 'close', 'volume']]
            df_list.append(df)
            i+=1
            if i % 10 == 0:
                positions = self.check_position(symbol, tp, sl)
            
            time.sleep(0.1)

        df2 = pd.concat(df_list)
        df_time = df2['time'].to_numpy()
        df_time = df_time.astype(float)
        df_time = df_time/1000
        df_time = np.round(df_time, decimals=0)
        df_time = df_time.astype(int)
        dftime = []
        for i in range(len(df_time)):
            dftime.append(dt.datetime.fromtimestamp(df_time[i], tz).strftime("%Y-%m-%d %H:%M"))
        x = pd.Series(dftime)
        df2['time'] = x.to_list()
        df2 = df2.iloc[::-1]
        df2.set_index('time', drop=True, inplace=True)
        df2['open'] = pd.to_numeric(df2['open'], downcast="float")
        df2['high'] = pd.to_numeric(df2['high'], downcast="float")
        df2['low'] = pd.to_numeric(df2['low'], downcast="float")
        df2['close'] = pd.to_numeric(df2['close'], downcast="float")
        df2['volume'] = pd.to_numeric(df2['volume'], downcast="float")

        return df2

    
