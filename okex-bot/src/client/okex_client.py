import os
import toml
import okex.Account_api as Account







# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #

config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #



class client_instance:
    def __init__(self):
        self.api_key = config['API_CREDENTIALS']['api_key']
        self.api_secret = config['API_CREDENTIALS']['api_secret']


    def client(self):
        print('asdf')