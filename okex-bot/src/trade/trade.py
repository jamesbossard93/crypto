import pandas as pd
import time
from threading import Timer
import os
import pytz
import toml
import datetime as dt
from src.market_data.market_data import market_data
from src.strategy.indicator_strategies import macd_strategy
from src.strategy.indicator_strategies import bollinger_strategy
from src.api.trade_api import TradeAPI
from src.client.client import Client
from src.api.account_api import AccountAPI
from src.api.market_api import MarketAPI
config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)



# NEED  HELPER FUNCTIONS
# Set global variable emergency stop loss (if last 2 trades are stopped out)
esl_bool = False
esl_cooldown_bool = False
esl_counter = -1
#esl_time = ""
e_sl_switch = False
order_history = []
trade_history = pd.DataFrame()

class trade:
    def __init__(self):
        self.symbol = config['BOT_SETTINGS']['symbol']
        self.esl_bool = False
        esl_cooldown_bool = False
        esl_counter = -1
        self.esl_time = self.get_esl_time(self.symbol)
        e_sl_switch = False
        self.skip_amount = config['BOT_SETTINGS']['skip_amount']
        self.risk = config['TRADE_SETTINGS']['risk_percentage']
        self.run_esl = config['BOT_SETTINGS']['run_esl']
        self.api_key = config['API_CREDENTIALS']['api_key']
        self.api_secret = config['API_CREDENTIALS']['api_secret']
        self.api_passphrase = config['API_CREDENTIALS']['api_passphrase']
        self.leverage = config['TRADE_SETTINGS']['leverage']
        self.tp = float(config['TRADE_SETTINGS']['take_profit'])
        self.sl = float(config['TRADE_SETTINGS']['stop_loss'])
        self.stop_loss_balance = float(config['TRADE_SETTINGS']['stop_loss_balance'])
        self.client = Client(api_key=self.api_key, api_secret_key=self.api_secret, passphrase=self.api_passphrase)
        self.trade_client = TradeAPI(api_key=self.api_key, api_secret_key=self.api_secret, passphrase=self.api_passphrase)
        self.account = AccountAPI(api_key=self.api_key, api_secret_key=self.api_secret, passphrase=self.api_passphrase)
        self.market = MarketAPI(api_key=self.api_key, api_secret_key=self.api_secret, passphrase=self.api_passphrase)
    def actual_time(self):
        # datetime object containing current date and time
        now = dt.datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        #print("date and time =", dt_string)
        return str(dt_string)

    def sync_60sec(self, op):
        info_time_new = dt.datetime.strptime(str(self.actual_time()), '%d/%m/%Y %H:%M:%S')
        waiting_time = 60 - info_time_new.second

        t = Timer(waiting_time, op)
        t.start()

        print(self.actual_time(), f'waiting till next minute and 00 sec...')

    def get_historical_trades(self, symbol):
        trades = TradeAPI(api_key="38fb0540-bbc1-40d6-a6f6-11e3cfb2d472", 
            api_secret_key="6BC0FE0BD26B9DDB14C175ED8320598F", passphrase="SZEQFCMwBugE2Xq")
        result = trades.get_order_history(instType="SWAP", instId=symbol)
        df = pd.DataFrame(result)
        df = df[['cTime','side', 'pnl']]
        df["pnl"] = pd.to_numeric(df["pnl"], downcast="float")
        df = df[df.pnl != 0]
        df = df.rename(columns={"cTime": "time"})
        df.set_index('time', inplace=True, drop=False)
        return df

    def get_num_positions(self):
        positions = self.account.positions()
        df = pd.DataFrame(positions)
        if df.empty:
            return 0
        else:
            return len(df)

    def get_esl_time(self, symbol):
        historical_trades = self.get_historical_trades(symbol)
        esl_time = historical_trades.index.values[-1]

        return esl_time

    def check_esl(self, historical_trades):
        pnl_list = []
        if historical_trades.empty or len(historical_trades) < 2:
            return False
        for i in historical_trades['pnl'].values:
            pnl_list.append(i)
        
        if pnl_list[0] < 0 and pnl_list[1] < 0:
            return True
        else:
            return False

    def check_tp_sl(self, symbol, tp, sl):

        df = self.account.positions()
        df = pd.DataFrame(df)
        if df.empty or df is None:
            return None
            
        #print("df: %s" % df)
        pnl = self.account.positions()[0]['upl']
        pnl = float(pnl)
        side = self.account.positions()[0]['posSide']
        tp = tp/(len(str(tp))+1)
        sl = sl/(len(str(sl))+1)
        closed = None
        print('Current pnl: %s' % pnl)
        if pnl >= 0.3:
            # close position
            closed = self.trade_client.close_positions(instId=symbol, 
                posSide=side, mgnMode='isolated')
            print("CLOSING LONG POSITION")
            print(closed)
            if closed != None:
                # do while loop here!
                while closed != None:
                    closed = self.check_tp_sl(symbol, self.tp, self.sl)         
                    time.sleep(1)
                    print("MONITORING OPEN POSITION...")
                else:
                    print("CLOSED POSITION")
                    print('EMERGENCY STOP LOSS ACTIVE')
                    esl_counter = 0

                    while esl_counter < self.skip_amount:
                        print('EMERGENCY STOP LOSS ACTIVE')
                        time.sleep(60)
                        esl_counter+=1
                        if esl_counter == self.skip_amount:
                            esl_counter = -1
                            esl_bool = False
                            break

        elif pnl <= -0.10:
            # close position
            closed = self.trade_client.close_positions(instId=symbol, 
                posSide=side, mgnMode='isolated')
            print("CLOSING SHORT POSITION")
            print(closed)
            if closed != None:
                # do while loop here!
                while closed != None:
                    closed = self.check_tp_sl(symbol, self.tp, self.sl)         
                    time.sleep(1)
                    print("MONITORING OPEN POSITION...")
                else:
                    print("CLOSED POSITION")
                    print('EMERGENCY STOP LOSS ACTIVE')
                    esl_counter = 0

                    while esl_counter < self.skip_amount:
                        print('EMERGENCY STOP LOSS ACTIVE')
                        time.sleep(60)
                        esl_counter+=1
                        if esl_counter == self.skip_amount:
                            esl_counter = -1
                            esl_bool = False
                            break
        else:
            return closed

    def open_trade(self, action, symbol, tp, sl):
        # get margin
        #print(df)
        # Get Bids and Asks
        orderbook = self.market.books(instId=symbol)
        ask = orderbook[0]['asks'][0][0]
        bid = orderbook[0]['bids'][0][0]
        num_digits = max([ask, bid])
        num_digits = num_digits[::-1].find('.')

        bid = float(bid)
        ask = float(ask)
        
        #tp = 10**(-num_digits)*tp
        #sl = 10**(-num_digits)*sl
        #tp = round(tp, num_digits)
        #sl = round(sl, num_digits)
        if action == "BUY":

            #print(num_digits)
            tp = 10**(-num_digits)*tp
            sl = 10**(-num_digits)*sl
            tp = tp+float(ask)
            tp = round(tp, num_digits)
            sl = float(ask)-sl
            sl = round(sl, num_digits)
            #tp = str(tp)
            #sl = str(sl)
            print('tp: %s' % tp)
            print('sl: %s' % sl)
            #result = self.trade_client.place_algo_order(instId=symbol, tdMode='isolated', 
            #side='buy', sz=1, ordType='conditional', posSide='long',
            #reduceOnly='false', tpTriggerPx=tp, tpOrdPx='-1', 
            #slTriggerPx=sl, slOrdPx='-1', triggerPx=ask, orderPx='-1')
            result = self.trade_client.order(instId=symbol, tdMode='isolated', 
                sz=1, ordType='market', posSide='long')

            #print(result)
            #result = result[0]['result']
            #print(result)
            return result

        elif action == "SELL":

            tp = 10**(-num_digits)*tp
            sl = 10**(-num_digits)*sl
            tp = float(bid)-tp
            tp = round(tp, num_digits)
            sl = sl+float(bid)
            sl = round(sl, num_digits)
            #tp = str(tp)
            #sl = str(sl)
            print('tp: %s' % tp)
            print('sl: %s' % sl)

            #result = self.trade_client.place_algo_order(instId=symbol, tdMode='isolated', 
            #side='sell', sz=1, ordType='conditional', posSide='short',
            #reduceOnly='false', tpTriggerPx=tp, tpOrdPx='-1', 
            #slTriggerPx=sl, slOrdPx='-1', triggerPx=bid, orderPx='-1')
            result = self.trade_client.order(instId=symbol, tdMode='isolated', 
                sz=-1, ordType='market', posSide='short')

            return result

    def esl_bot(self, symbol, tp, sl):
        # the main trading bot

        # set global variables
        global esl_bool
        global esl_time
        global esl_counter
        global esl_cooldown_bool
        global order_history
        global trade_history

        # first test connection here.
        #print(self.client.Common.Common_getTime().result()[0])
        tz = pytz.timezone("UTC")
        trade_client = TradeAPI(api_key=self.api_key, api_secret_key=self.api_secret, passphrase=self.api_passphrase)
        time_now = int(float(trade_client.time_now()[0]['ts'])/1000)
        time_now = dt.datetime.fromtimestamp(time_now, tz).strftime("%Y-%m-%d %H:%M")
        time_now = dt.datetime.strptime(time_now, "%Y-%m-%d %H:%M")#+daylight_savings
        print("Current UTC time is: %s" % time_now)
        # get data

        #df = market_data().get_kline_bars(symbol)
        df = market_data().get_kline_bars_trade(symbol=symbol, tp=tp, sl=sl)
        time.sleep(1)
        print('Last 5 minutes of data:')
        print(df.tail(5))
        closed_order = self.check_tp_sl(symbol, self.tp, self.sl)
        if closed_order != None:
            # do while loop here!
            while closed_order != None:
                closed_order = self.check_tp_sl(symbol, self.tp, self.sl)         
                time.sleep(1)
                print("MONITORING OPEN POSITION...")
            else:
                print("CLOSED POSITION")
                print('EMERGENCY STOP LOSS ACTIVE')
                esl_counter = 0

                while esl_counter < self.skip_amount:
                    print('EMERGENCY STOP LOSS ACTIVE')
                    time.sleep(60)
                    esl_counter+=1
                    if esl_counter == self.skip_amount:
                        esl_counter = -1
                        esl_bool = False
                        break
                
            print(closed_order)
        else:
            print("NO OPEN POSITION")
        print(f"\n", self.actual_time(),f"|| waiting for signals {symbol} ||\n")

        #------------------------------------------------------------------------------
        #                   E M E R G E N C Y   S T O P - L O S S 
        #------------------------------------------------------------------------------

        # get historical trades
        historical_trades = self.get_historical_trades(symbol)
        if historical_trades.empty:
            esl_bool = False
        else:
            esl_bool = self.check_esl(historical_trades)
        time.sleep(1)

        if esl_bool == True and esl_time != self.get_esl_time(symbol):
            # activate emergency stop loss
            # ONLY ACTIVATES ONCE, NEED TO FIX THIS!!!
            print('EMERGENCY STOP LOSS ACTIVE')
            esl_counter+=1
            esl_cooldown_bool = True
            if esl_counter >= self.skip_amount:
                esl_bool = False
                esl_time = self.get_esl_time(symbol)
                esl_counter = -1
                esl_cooldown_bool = False
                print("EMERGENCY STOP LOSS INACTIVE, RESUMING TRADING MODE")
                pass

            else:
                esl_counter = -1
                esl_bool = False
                pass

        #------------------------------------------------------------------------------
        #                   A P P L Y   S T R A T E G Y 
        #------------------------------------------------------------------------------

        #signals = macd_strategy().check_macd_with_stochrsi(df)
        signals = bollinger_strategy().simple_bollinger_2(df)

        # Get positions
        num_positions = self.get_num_positions()

        if num_positions > 0:
            pass
        elif esl_counter < 0:
            print("SEARCHING FOR SIGNALS...")
            if len(signals) > 3:
                print("LAST 3 STRATEGY SIGNALS: %s" % signals[-3:])
            # NOW CHECK FOR BUY OR SELL SIGNALS

            if signals[-1] == "BUY":
                print('BUY SIGNAL ACTIVATED!')
                result = self.open_trade("BUY", symbol, self.tp, self.sl)
                esl_time = self.get_esl_time(symbol)
                print(result)
                order_history.append(result)
                esl_bool = True
            elif signals[-1] == "SELL":
                print('SELL SIGNAL ACTIVATED!')
                result = self.open_trade("SELL", symbol, self.tp, self.sl)
                esl_time = self.get_esl_time(symbol)
                print(result)      
                order_history.append(result)   
                esl_bool = True       
            else:
                pass
        else:
            pass

    def without_esl_bot(self, symbol):
        # the main trading bot

        # first test connection here.
        #print(self.client.Common.Common_getTime().result()[0])
        tz = pytz.timezone("UTC")
        trade_client = TradeAPI(api_key=self.api_key, api_secret_key=self.api_secret, passphrase=self.api_passphrase)
        time_now = int(float(trade_client.time_now()[0]['ts'])/1000)
        time_now = dt.datetime.fromtimestamp(time_now, tz).strftime("%Y-%m-%d %H:%M")
        time_now = dt.datetime.strptime(time_now, "%Y-%m-%d %H:%M")#+daylight_savings
        print("Current UTC time is: %s" % time_now)
        # get data
        closed_order = self.check_tp_sl(symbol, self.tp, self.sl)
        if closed_order != None:
            print("CLOSED POSITION")
            print(closed_order)
        else:
            print("NO OPEN POSITION")


        df = market_data().get_kline_bars(symbol)
        print('Last 5 minutes of data:')
        print(df.tail(5))

        print(f"\n", self.actual_time(),f"|| waiting for signals {symbol} ||\n")




        #------------------------------------------------------------------------------
        #                   A P P L Y   S T R A T E G Y 
        #------------------------------------------------------------------------------
        #df = df*1000
        #signals = macd_strategy().check_macd_with_stochrsi(df)
        signals = bollinger_strategy().simple_bollinger_1(df, b_band_delta=0.08)
        #df = df/1000
        # Get positions
        num_positions = self.get_num_positions()

        if num_positions > 0:
            pass
        else:
            print("SEARCHING FOR SIGNALS...")
            if len(signals) > 3:
                print("LAST 3 STRATEGY SIGNALS: %s" % signals[-3:])
            # NOW CHECK FOR BUY OR SELL SIGNALS

            if signals[-1] == "BUY":
                print('BUY SIGNAL ACTIVATED!')
                result = self.open_trade("BUY", symbol, self.tp, self.sl)
                esl_time = self.get_esl_time(symbol)
                print(result)
                order_history.append(result)

            elif signals[-1] == "SELL":
                print('SELL SIGNAL ACTIVATED!')
                result = self.open_trade("SELL", symbol, self.tp, self.sl)
                esl_time = self.get_esl_time(symbol)
                print(result)      
                order_history.append(result)          
            else:
                pass



    def run_trade(self, symbol):
        #print('test')
        #historical_df = self.get_historical_pnl(symbol)
        balance = self.account.balance()[0]['details'][0]['disEq']
        balance = float(balance)
        balance = round(balance, 4)
        if balance < self.stop_loss_balance:
            print("BALANCE REACHED CRITICAL LEVEL!")
            print("QUITTING BOT...")
            exit()
        global esl_time
        order_history = []
        esl_time = self.get_esl_time(symbol)
        tp = self.tp
        sl = self.sl
        while True:
            try:
                if self.run_esl is True:
                    self.esl_bot(symbol, tp, sl)
                    closed_order = self.check_tp_sl(symbol, tp, sl)
                    print(closed_order)
                else:
                    self.without_esl_bot(symbol)
                    closed_order = self.check_tp_sl(symbol, tp, sl)
                    print(closed_order)
    
            except KeyboardInterrupt:
                current_time = dt.datetime.now()
                current_time = str(int(dt.datetime.timestamp(current_time)))
                order_history = self.get_historical_trades(symbol)
                #order_history = pd.DataFrame(order_history)
                order_history.to_csv('order_history_%s.csv' % current_time)
                break