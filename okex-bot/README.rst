.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/okex-py.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/okex-py
    .. image:: https://readthedocs.org/projects/okex-py/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://okex-py.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/okex-py/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/okex-py
    .. image:: https://img.shields.io/pypi/v/okex-py.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/okex-py/
    .. image:: https://img.shields.io/conda/vn/conda-forge/okex-py.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/okex-py
    .. image:: https://pepy.tech/badge/okex-py/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/okex-py
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/okex-py

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

=======
okex-py
=======


    Test okex api


A longer description of your project goes here...


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
