import numpy as np
import decimal
import pandas as pd
import dateparser
import matplotlib.pyplot as plt
from binance.client import Client
from binance.enums import *
binance_api_key = "API KEY"
binance_api_secret = "API SECRET"

client = Client(binance_api_key, 
    binance_api_secret)
#candles = binance_client.get_klines(symbol='BNBBTC', 
#    interval=binance.client.Client.KLINE_INTERVAL_1HOUR)

def preprocess_klines(klines_data):
    #returns only close and volume
    tmp = []
    for i in klines_data:
        tmp.append([float(i[1]), float(i[2]),
            float(i[3]), float(i[4]), float(i[5])])

    return tmp



def hour_confirmation(ticker_pair):

    candles = client.get_klines(symbol=ticker_pair, 
        interval=client.KLINE_INTERVAL_1HOUR)
    
    tmp = preprocess_klines(candles)
    tmp = pd.DataFrame(tmp, columns=['open', 'high', 'low', 'close',
        'volume'])
    return tmp


def five_min_data(ticker_pair):

    candles = client.get_klines(symbol=ticker_pair, 
        interval=client.KLINE_INTERVAL_5MINUTE)
    
    tmp = preprocess_klines(candles)
    tmp = pd.DataFrame(tmp, columns=['open', 'high', 'low', 'close',
        'volume'])
    return tmp


def get_ema(ema_num, ticker_data):
    #tmp = pd.DataFrame(ticker_data, columns=['open', 'high', 'low', 'close',
    #    'volume'])
    ticker_data = ticker_data['close'].ewm(span=ema_num, adjust=False).mean()

    return ticker_data

def check_buy_signal(ticker_data, ema_8, ema_21, hourly_range):

    for i in range((len(ticker_data)-hourly_range), len(ticker_data)):
        if ema_21[i] < ema_8[i] < ticker_data['low'][i]:
            continue
        else:
            return "NO BUY"
    
    return "BUY"

def check_sell_signal(ticker_data, ema_8, ema_21, hourly_range):

    for i in range((len(ticker_data)-hourly_range), len(ticker_data)):
        print("ticker_data high: %s" % ticker_data['high'][i])
        print("ema_8: %s" % ema_8[i])
        print("ema_21: %s" % ema_21[i])
        if ticker_data['high'][i] < ema_8[i] < ema_21[i]:
            continue
        else:
            return "NO SELL"
    return "SELL"




def check_hourly_confirmation(ticker_data):

    ema_8 = get_ema(8, ticker_data)
    ema_21 = get_ema(21, ticker_data)
    #print(ticker_data['open'][0])
    #print("ema 8 length: %s" % len(ema_8))
    #print("ema 21 length: %s" % len(ema_21))
    #print("low length: %s" % len(ticker_data['low']))
    #print(check_buy_signal(ticker_data, ema_8, ema_21, 12))
    print(check_sell_signal(ticker_data, ema_8, ema_21, 12))
    tmp2 = range(500)
    plt.plot(tmp2, ema_8, "r", label='8 EMA', linewidth=3)
    plt.plot(tmp2, ema_21, "b", label='21 EMA', linewidth=4)
    plt.plot(tmp2, ticker_data['low'], "k", label='Close', linewidth=2)
    plt.legend(loc='upper left')
    plt.show()

    #for i in range((len(ticker_data)-12), len(ticker_data)):
    #    if ema_8[i]
#def buy_ticker():


#def short_sell():
"""     transaction = client.margin_create_loan(asset='BTC', amount='1.1')

transaction = client.margin_create_loan(asset='BTC', amount='1.1', 
                                        isIsolated='TRUE', symbol='ETHBTC') """
    

#def take_profit():
    #return Buy or Sell?
# Example candles
""" [
    [
        1499040000000,      # Open time
        "0.01634790",       # Open
        "0.80000000",       # High
        "0.01575800",       # Low
        "0.01577100",       # Close
        "148976.11427815",  # Volume
        1499644799999,      # Close time
        "2434.19055334",    # Quote asset volume
        308,                # Number of trades
        "1756.87402397",    # Taker buy base asset volume
        "28.46694368",      # Taker buy quote asset volume
        "17928899.62484339" # Can be ignored
    ]
]
 """

asdf = hour_confirmation("BNBBTC")
tmp = client.get_recent_trades(symbol="BTCUSDT")
print(tmp[0])
print(tmp[-1:])

#print(get_ema(8, asdf)) #????????
#print(check_hourly_confirmation(asdf))
#print(len(candles))
#print(candles[0])
