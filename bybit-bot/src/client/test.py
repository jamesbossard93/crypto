import os
import sys
import toml



path_parent = os.path.dirname(os.path.dirname(os.getcwd()))
config_dir = os.path.dirname(os.path.dirname(os.getcwd())) + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)


print(config['API_CREDENTIALS']['api_key'])

start_time = config['BOT_SETTINGS']['start_time']

print(type(start_time))