import time
from pandas._libs.tslibs import timestamps
import bybit
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
import datetime as dt
import pandas as pd
import os
import toml

# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #


config_dir = os.getcwd() + "\config\config.toml"
print("config_dir: %s" % config_dir)
with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #



# ---------------------------------------------------------------------------- #
#                            INITIALIZE BYBIT CLIENT                           #
# ---------------------------------------------------------------------------- #

class client_instance:
    def __init__(self):
        self.configuration = swagger_client.Configuration()
        # Configure API key authorization: apiKey
        self.configuration.api_key['api_key'] = config['API_CREDENTIALS']['api_key']
        self.configuration.api_key['secret'] = config['API_CREDENTIALS']['api_secret']
        self.api_key = config['API_CREDENTIALS']['api_key']
        self.api_secret = config['API_CREDENTIALS']['api_secret']

    def client(self):
        client = bybit.bybit(test=False, api_key=self.api_key, api_secret=self.api_secret)
        return client

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #


# May also need to add client account info functions here.