import pandas as pd
import time
from threading import Timer
import bybit
import os
import pytz
import toml
import datetime as dt
from src.market_data.market_data import market_data
from src.strategy.indicator_strategies import macd_strategy
from src.strategy.indicator_strategies import bollinger_strategy
from src.client.bybit_client import client_instance
config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)



# NEED  HELPER FUNCTIONS
# Set global variable emergency stop loss (if last 2 trades are stopped out)
esl_bool = False
esl_cooldown_bool = False
esl_counter = -1
#esl_time = ""
e_sl_switch = False
order_history = []


class trade:
    def __init__(self):
        self.symbol = config['BOT_SETTINGS']['symbol']
        self.client = client_instance()
        self.esl_bool = False
        esl_cooldown_bool = False
        esl_counter = -1
        self.esl_time = self.get_esl_time(self.symbol)
        e_sl_switch = False
        self.risk = config['TRADE_SETTINGS']['risk_percentage']
        # DO THIS !!!!!!!!!!!!
        self.leverage = config['TRADE_SETTINGS']['leverage']
        self.tp = config['TRADE_SETTINGS']['take_profit']
        self.sl = config['TRADE_SETTINGS']['stop_loss']



    def actual_time(self):
        # datetime object containing current date and time
        now = dt.datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        #print("date and time =", dt_string)
        return str(dt_string)

    def sync_60sec(self, op):
        info_time_new = dt.datetime.strptime(str(self.actual_time()), '%d/%m/%Y %H:%M:%S')
        waiting_time = 60 - info_time_new.second

        t = Timer(waiting_time, op)
        t.start()

        print(self.actual_time(), f'waiting till next minute and 00 sec...')

    def get_historical_trades(self, symbol):
        historical_trades = self.client.client().LinearPositions.LinearPositions_closePnlRecords(symbol=symbol).result()
        historical_trades = historical_trades[0]['result']['data']     
        df = pd.DataFrame(historical_trades)
        df = df[['symbol', 'side', 'closed_pnl', 'created_at']]
        df.set_index('created_at', drop=True, inplace=True)
        is_symbol = df['symbol'] == symbol
        df = df[is_symbol]
        
        return df

    def get_positions(self, symbol):
        positions = self.client.client().LinearPositions.LinearPositions_myPosition(symbol=symbol).result()[0]['result']
        df = pd.DataFrame(positions)

        position_size =  df['size'] != 0
        df = df[position_size]

        if df.empty:
            return 0
        else:
            return len(df)


        print('current positions: %s' % len(positions))


    def get_esl_time(self, symbol):
        historical_trades = self.get_historical_trades(symbol)
        esl_time = historical_trades.index.values[-1]

        return esl_time

    def check_esl(self, historical_trades):
        pnl_list = []
        for i in historical_trades['closed_pnl'].values:
            pnl_list.append(i)
        
        if pnl_list[0] < 0 and pnl_list[1] < 0:
            return True
        else:
            return False


    def open_trade(self, action, symbol, tp, sl):
        # get margin
        balance = self.client.client().Wallet.Wallet_getBalance(coin="USDT").result()[0]
        margin = self.risk * float(balance['result']['USDT']['available_balance'])

        # MAY NEED TO FIX ORDER_AMOUNT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        order_amount = round(self.leverage*margin)
        orderbook = self.client.client().Market.Market_orderbook(symbol="ADAUSDT").result()[0]['result']
        df = pd.DataFrame(orderbook)
        #print(df)
        # Get Bids and Asks
        buys = df['side'] == "Buy"
        bids = df[buys]
        bids = bids['price'].values 

        sells = df['side'] == 'Sell'
        asks = df[sells]
        asks = asks['price'].values
        df = pd.DataFrame(self.client.client().Symbol.Symbol_get().result()[0]["result"])
        price_scale = df.loc[df['name'] == symbol]
        price_scale = float(price_scale['price_scale'].values[0])

        if action == "BUY":

            min_ask = min(asks)
            min_ask = str(min_ask)
            #print(num_digits)
            tp = 10**(-price_scale)*tp
            sl = 10**(-price_scale)*sl
            #print('tp: %s' % tp)
            #print('tp: %s' % tp)
            price_scale = int(price_scale)
            tp = tp+float(min_ask)
            tp = round(tp, price_scale)
            sl = float(min_ask)-sl
            sl = round(sl, price_scale)
            tp = str(tp)
            sl = str(sl)
            print('tp: %s' % tp)
            print('sl: %s' % sl)
            result = self.client.client().LinearOrder.LinearOrder_new(side="Buy",symbol=symbol,
                order_type="Market",qty=order_amount, take_profit=tp, stop_loss=sl,
                time_in_force="GoodTillCancel", reduce_only=False, close_on_trigger=False, 
                tp_trigger_by="MarkPrice", sl_trigger_by="MarkPrice").result()
            print(result)
            result = result[0]['result']
            #print(result)
            return result

        elif action == "SELL":

            min_ask = min(asks)
            min_ask = str(min_ask)
            tp = 10**(-price_scale)*tp
            sl = 10**(-price_scale)*sl
            price_scale = int(price_scale)
            tp = float(min_ask)-tp
            tp = round(tp, price_scale)
            sl = sl+float(min_ask)
            sl = round(sl, price_scale)
            tp = str(tp)
            sl = str(sl)
            print('tp: %s' % tp)
            print('sl: %s' % sl)

            result = self.client.client().LinearOrder.LinearOrder_new(side="Sell",symbol=symbol,
                order_type="Market",qty=order_amount, take_profit=tp, stop_loss=sl,
                time_in_force="GoodTillCancel", reduce_only=False, close_on_trigger=False, 
                tp_trigger_by="MarkPrice", sl_trigger_by="MarkPrice").result()
                
            return result

    def esl_bot(self, symbol):
        # the main trading bot

        # first test connection here.
        #print(self.client.Common.Common_getTime().result()[0])
        tz = pytz.timezone("UTC")

        time_now = self.client.client().Common.Common_getTime().result()[0]['time_now']
        time_now = float(time_now)
        time_now = dt.datetime.fromtimestamp(time_now, tz).strftime("%Y-%m-%d %H:%M")
        time_now = dt.datetime.strptime(time_now, "%Y-%m-%d %H:%M")#+daylight_savings
        print("Current UTC time is: %s" % time_now)
        # get data
        # 


        df = market_data().get_kline_bars(symbol)
        print('Last 5 minutes of data:')
        print(df.tail(5))

        print(f"\n", self.actual_time(),f"|| waiting for signals {symbol} ||\n")

        #------------------------------------------------------------------------------
        #                   E M E R G E N C Y   S T O P - L O S S 
        #------------------------------------------------------------------------------

        # set global variables
        global esl_bool
        global esl_time
        global esl_counter
        global esl_cooldown_bool
        global order_history
        # get historical trades
        historical_trades = self.get_historical_trades(symbol)
        esl_bool = self.check_esl(historical_trades)


        if esl_bool == True and esl_time != self.get_esl_time(symbol):
            # activate emergency stop loss
            # ONLY ACTIVATES ONCE, NEED TO FIX THIS!!!
            print('EMERGENCY STOP LOSS ACTIVE')
            esl_counter+=1
            esl_cooldown_bool = True
            if esl_counter >= 19:
                esl_bool = False
                esl_time = self.get_esl_time(symbol)
                esl_counter = -1
                esl_cooldown_bool = False
                print("EMERGENCY STOP LOSS INACTIVE, RESUMING TRADING MODE")
                pass

            else:
                esl_counter = -1
                esl_bool = False
                pass

        #------------------------------------------------------------------------------
        #                   A P P L Y   S T R A T E G Y 
        #------------------------------------------------------------------------------

        signals = macd_strategy().check_macd_with_stochrsi(df)

        # Get positions
        num_positions = self.get_positions(symbol)

        if num_positions > 0:
            pass
        elif esl_counter < 0:
            print("SEARCHING FOR SIGNALS...")
            if len(signals) > 3:
                print("LAST 3 STRATEGY SIGNALS: %s" % signals[-3:])
            # NOW CHECK FOR BUY OR SELL SIGNALS

            if signals[-1] == "BUY":
                print('BUY SIGNAL ACTIVATED!')
                result = self.open_trade("BUY", symbol, self.tp, self.sl)
                esl_time = self.get_esl_time(symbol)
                print(result)
                order_history.append(result)

            elif signals[-1] == "SELL":
                print('SELL SIGNAL ACTIVATED!')
                result = self.open_trade("SELL", symbol, self.tp, self.sl)
                esl_time = self.get_esl_time(symbol)
                print(result)      
                order_history.append(result)          
            else:
                pass
        else:
            pass

    def without_esl_bot(self, symbol):
        # the main trading bot

        # first test connection here.
        #print(self.client.Common.Common_getTime().result()[0])
        tz = pytz.timezone("UTC")

        time_now = self.client.client().Common.Common_getTime().result()[0]['time_now']
        time_now = float(time_now)
        time_now = dt.datetime.fromtimestamp(time_now, tz).strftime("%Y-%m-%d %H:%M")
        time_now = dt.datetime.strptime(time_now, "%Y-%m-%d %H:%M")#+daylight_savings
        print("Current UTC time is: %s" % time_now)
        # get data
        #


        df = market_data().get_kline_bars(symbol)
        print('Last 5 minutes of data:')
        print(df.tail(5))

        print(f"\n", self.actual_time(),f"|| waiting for signals {symbol} ||\n")




        #------------------------------------------------------------------------------
        #                   A P P L Y   S T R A T E G Y 
        #------------------------------------------------------------------------------

        signals = macd_strategy().check_macd_with_stochrsi(df)

        # Get positions
        num_positions = self.get_positions(symbol)

        if num_positions > 0:
            pass
        else:
            print("SEARCHING FOR SIGNALS...")
            if len(signals) > 3:
                print("LAST 3 STRATEGY SIGNALS: %s" % signals[-3:])
            # NOW CHECK FOR BUY OR SELL SIGNALS

            if signals[-1] == "BUY":
                print('BUY SIGNAL ACTIVATED!')
                result = self.open_trade("BUY", symbol, self.tp, self.sl)
                esl_time = self.get_esl_time(symbol)
                print(result)
                order_history.append(result)

            elif signals[-1] == "SELL":
                print('SELL SIGNAL ACTIVATED!')
                result = self.open_trade("SELL", symbol, self.tp, self.sl)
                esl_time = self.get_esl_time(symbol)
                print(result)      
                order_history.append(result)          
            else:
                pass



    def run_trade(self, symbol):
        #print('test')
        #historical_df = self.get_historical_pnl(symbol)
        global esl_time
        order_history = []
        esl_time = self.get_esl_time(symbol)
        while True:
            try:
                self.esl_bot(symbol)
            except KeyboardInterrupt:
                current_time = dt.datetime.now()
                current_time = str(int(dt.datetime.timestamp(current_time)))
                order_history = self.get_historical_trades(symbol)
                #order_history = pd.DataFrame(order_history)
                order_history.to_csv('order_history_%s.csv' % current_time)
                break