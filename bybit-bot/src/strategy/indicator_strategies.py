import math
import toml
import os
import datetime as dt
import pandas as pd
import time

# TA library imports
from ta.trend import ema_indicator
from ta.trend import macd_diff
from ta.momentum import StochRSIIndicator
from ta.volatility import BollingerBands

# Project Imports
from src.market_data.market_data import market_data
from src.bybit_api_bot import helper_functions

# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #

config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #



class macd_strategy:
    def __init__(self):
        pass

    # ---------------------------------------------------------------------------- #
    #                      MACD HISTOGRAM WITH STOCHASTIC RSI                      #
    # ---------------------------------------------------------------------------- #

    def check_macd_with_stochrsi(self, df):
        # data = pandas dataframe of all indicators including close price
        # window = range/bars to look
        # rsi_delta = the error term when looking at the rsi levels.
        # between close price and ema
        # checks for stochastic crossover while above 0.8 or below 0.2
        close_data = df['close']
        # Steps:
        # 1. Check if ema is uptrend or downtrend.
        # 2. if so, look for when stochrsi <= 0.2 + rsi_delta
        # 3. confirm move with MACD histogram (diff).
        macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
        ema = ema_indicator(close_data, window=50, fillna=False)
        # Use stoch_rsi_d to search for divergences and find entry points
        stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
            smooth1=1, smooth2=1, fillna=False).stochrsi_d()
        stoch_rsi_d = stoch_rsi_d.dropna()

        #print(stoch_rsi_d)
        #ema = pd.DataFrame(ema)
        min_list = min([len(stoch_rsi_d), len(ema), len(macd_histogram), len(close_data)])
        # ---------------------------------------------------------------------------------------------------------
        close_data = close_data.tail(min_list)
        macd_histogram = macd_histogram.tail(min_list)
        ema = ema.tail(min_list)
        stoch_rsi_d = stoch_rsi_d.tail(min_list)

        window_list = []

        for i in range(min_list):

            #first check if ema is uptrend or downtrend
            if math.exp(ema[i]-ema[i-1]) < 1:
                # if ema is increasing
                if stoch_rsi_d[i] >= 0.6:
                    if macd_histogram[i] >= 0.05:
                        window_list.append("SELL")
                    else:
                        window_list.append("IGNORE")
                elif stoch_rsi_d[i] <= 0.4:
                    if macd_histogram[i] <= -0.05:
                        window_list.append("BUY")
                    else:
                        window_list.append("IGNORE")

                else:
                    window_list.append("IGNORE")

            elif math.exp(ema[i]-ema[i-1]) > 1:
                # if ema is decreasing
                if stoch_rsi_d[i] >= 0.6:
                    if macd_histogram[i] >= 0.05:
                        window_list.append("SELL")
                    else:
                        window_list.append("IGNORE")
                elif stoch_rsi_d[i] <= 0.4:
                    if macd_histogram[i] <= -0.05:
                        window_list.append("BUY")
                    else:
                        window_list.append("IGNORE")

                else:
                    window_list.append("IGNORE")
            else:
                window_list.append("IGNORE")

        return window_list


    # ---------------------------------------------------------------------------- #
    #                      MACD HISTOGRAM WITH STOCHASTIC RSI                      #
    # ---------------------------------------------------------------------------- #

    def check_macd_with_stochrsiv2(self, df):
        # data = pandas dataframe of all indicators including close price
        # window = range/bars to look
        # rsi_delta = the error term when looking at the rsi levels.
        # between close price and ema
        # checks for stochastic crossover while above 0.8 or below 0.2
        close_data = df['close']
        # Steps:
        # 1. Check if ema is uptrend or downtrend.
        # 2. if so, look for when stochrsi <= 0.2 + rsi_delta
        # 3. confirm move with MACD histogram (diff).
        macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
        ema = ema_indicator(close_data, window=50, fillna=False)
        trend = self.trend.ema_trend(20)
        # Use stoch_rsi_d to search for divergences and find entry points
        stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
            smooth1=1, smooth2=1, fillna=False).stochrsi_d()
        stoch_rsi_d = stoch_rsi_d.dropna()

        #print(stoch_rsi_d)
        #ema = pd.DataFrame(ema)
        min_list = min([len(stoch_rsi_d), len(trend), len(macd_histogram), len(close_data)])
        # ---------------------------------------------------------------------------------------------------------
        close_data = close_data.tail(min_list)
        macd_histogram = macd_histogram.tail(min_list)
        trend = trend[-min_list:]
        stoch_rsi_d = stoch_rsi_d.tail(min_list)

        window_list = []

        for i in range(min_list):

            #first check if ema is uptrend or downtrend
            if trend[i] == "UPTREND":
                # if ema is increasing
                if stoch_rsi_d[i] >= 0.6:
                    if macd_histogram[i] >= 0.05:
                        window_list.append("SELL")
                    else:
                        window_list.append("IGNORE")
                elif stoch_rsi_d[i] <= 0.4:
                    if macd_histogram[i] <= -0.05:
                        window_list.append("BUY")
                    else:
                        window_list.append("IGNORE")

                else:
                    window_list.append("IGNORE")

            elif trend[i] == "DOWNTREND":
                # if ema is decreasing
                if stoch_rsi_d[i] >= 0.6:
                    if macd_histogram[i] >= 0.05:
                        window_list.append("SELL")
                    else:
                        window_list.append("IGNORE")
                elif stoch_rsi_d[i] <= 0.4:
                    if macd_histogram[i] <= -0.05:
                        window_list.append("BUY")
                    else:
                        window_list.append("IGNORE")

                else:
                    window_list.append("IGNORE")
            else:
                window_list.append("IGNORE")

        return window_list


class bollinger_strategy:
    def __init__(self):
        self.df = market_data.get_kline_bars()
        self.trend = helper_functions.trends()
    # ---------------------------------------------------------------------------- #
    #       TRIPLE THREAT STRATEGY: STOCH_RSI+MACD_HISTOGRAM+BOLLINGER_BANDS       #
    # ---------------------------------------------------------------------------- #

    def stochrsi_macd_bollinger_indicator(self, df, b_band_delta):
        ''' Requires bollinger band delta as input.'''
        # THIS STRATEGY IS A CONTINUATION TREND STRATEGY.
        # IT DOES NOT TRY TO 'CHASE' THE TOPS AND BOTTOMS LIKE
        # MOST STRATEGIES.

        close_data = df['close']
        #close_data = close_data.iloc[200:]

        # trend analysis
        #trend = ema_ribbon_trend(close_data)
        #trend = ema_trend(close_data, 10)
        trend = self.trend.ema_trend(200)
        macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
        stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
            smooth1=1, smooth2=1, fillna=False).stochrsi_d()
        stoch_rsi_d = stoch_rsi_d.dropna()
        hband = BollingerBands(close_data, 20, 3, False).bollinger_hband()
        lband = BollingerBands(close_data, 20, 3, False).bollinger_lband()
        mband = BollingerBands(close_data, 20, 3, False).bollinger_mavg()
        min_list = min([len(stoch_rsi_d), len(hband), len(macd_histogram), len(close_data), len(trend)])

        df_times = df.index.to_numpy()
        df_times = pd.Series(df_times).tail(min_list).to_list()

        close_data = close_data.tail(min_list).to_list()
        macd_histogram = macd_histogram.tail(min_list).to_list()
        stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()
        hband = hband.tail(min_list).to_list()
        lband = lband.tail(min_list).to_list()
        mband = mband.tail(min_list).to_list()
        # this gives better results for some reason?
        #trend = trend[:min_list]
        trend = pd.Series(trend).tail(min_list).to_list()
        #trend = trend[-min_list:]
        # ---------------------------------------------------------------------------- #
        #                                     LISTS                                    #
        # ---------------------------------------------------------------------------- #

        series_list = [len(trend), len(lband), len(hband), len(mband), len(stoch_rsi_d),
            len(macd_histogram), len(close_data), len(df_times)]

        lband_len = len(lband) - min(series_list)
        trend_len = len(trend) - min(series_list)
        macd_len = len(macd_histogram) - min(series_list)
        stoch_len = len(stoch_rsi_d) - min(series_list)
        close_len = len(close_data) - min(series_list)
        times_len = len(df_times) - min(series_list)
        if lband_len > 0:
            hband = hband[lband_len:]
            lband = lband[lband_len:]
            mband = mband[lband_len:]

        if trend_len > 0:
            trend = trend[trend_len:]
        if macd_len > 0:
            macd_histogram = macd_histogram[macd_len:]
        if close_len > 0:
            close_data = close_data[close_len:]
        if stoch_len > 0:
            stoch_rsi_d = stoch_rsi_d[stoch_len:]
        if times_len > 0:
            df_times = df_times[times_len:]

        test_list = []
        results_list = []

        #from_time = current_tick_time-timedelta(hours=3)
        #time_now = datetime.now(timezone)+daylight_savings
        ######### Change here the timeframe
        #hourly_df = mt5.copy_rates_range(symbol, mt5.TIMEFRAME_H1, from_time, time_now)

        for i in range(min_list):
            #current_tick_time = df_times[i]
            #from_time = current_tick_time-timedelta(hours=3)
            #trend =
            #from_time = current_tick_time-timedelta(days=2)
            #time_now = datetime.now(timezone)+daylight_savings

            # first do something
            delta_lband = abs(lband[i] - close_data[i])
            delta_hband = abs(hband[i] - close_data[i])
            delta_mband = abs(mband[i] - close_data[i])
            # 
            #print("delta_lband: %s" % delta_lband)
            #print("delta_hband: %s" % delta_hband)
            #print("delta_mband: %s" % delta_mband)
            # FIX THE DELTAS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # check ema trend, if uptrend-> look for buys etc..
            if trend[i] == "UPTREND":
                # check for pullback during uptrend
                if delta_mband <= b_band_delta:
                    if 0 < macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5:
                        # pullback BUY
                        results_list.append("BUY")
                        test_list.append(("LONG", df_times[i], close_data[i]))
                        continue
                    else:
                        results_list.append("IGNORE")
                        test_list.append(("IGNORE", df_times[i], close_data[i]))
                        continue
                elif lband[i] < close_data[i] < mband[i]:
                    if macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5: 
                        # pullback BUY
                        results_list.append("BUY")
                        test_list.append(("BUY", df_times[i], close_data[i]))
                        continue
                    else:
                        results_list.append("IGNORE")
                        test_list.append(("IGNORE", df_times[i], close_data[i]))
                        continue
                else:
                    results_list.append("IGNORE")
                    continue

            elif trend[i] == "DOWNTREND":
                # check for potential sells during pullback
                asdf = delta_mband
                if delta_mband <= b_band_delta:
                    if -0.2 < macd_histogram[i] < 0 and 0.3 <= stoch_rsi_d[i]:
                        # pullback SELL
                        results_list.append("SELL")
                        test_list.append(("SHORT", df_times[i], close_data[i]))
                        continue
                    else:
                        results_list.append("IGNORE")
                        test_list.append(("IGNORE", df_times[i], close_data[i]))
                        continue                    
                else:
                    results_list.append("IGNORE")
                    continue
            else:
                # SIDEWAYS MARKET
                # check macd and stochrsi here!
                
                if delta_hband <= b_band_delta:
                    # check macd and stochrsi for sell
                    if macd_histogram[i] >= 0 and stoch_rsi_d[i] > 0.5:
                        results_list.append("SELL")
                        test_list.append(("SHORT", df_times[i], close_data[i]))
                        continue
                    else:
                        results_list.append("IGNORE")
                        continue
                elif delta_lband <= b_band_delta:
                    if macd_histogram[i] <= 0 and stoch_rsi_d[i] < 0.5:
                        results_list.append("BUY")
                        test_list.append(("LONG", df_times[i], close_data[i]))
                        continue
                    else:
                        results_list.append("IGNORE")
                        continue
        # returns a list with the oldest data first.
        return results_list

