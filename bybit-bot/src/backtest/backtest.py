import os
import toml
import pandas as pd
pd.set_option('display.max_columns', 500) # number of columns to be displayed
pd.set_option('display.width', 1500)      # max table width to display
import plotly.graph_objs as go

pd.set_option('display.max_columns', 10) # number of columns to be displayed
pd.set_option('display.max_rows', 100)
pd.set_option('display.width', 1500)      # max table width to display

# Project Imports
from src.market_data.market_data import market_data
from src.bybit_api_bot import helper_functions
from src.strategy.indicator_strategies import macd_strategy
from src.strategy.indicator_strategies import bollinger_strategy

from src.client.bybit_client import client_instance
# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #

path_parent = os.getcwd()
config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #

class backtest_program:
    def __init__(self):
        self.symbol_list = config['BOT_SETTINGS']['symbol_list']
        self.symbol = config['BOT_SETTINGS']['symbol']
        self.tp = config['TRADE_SETTINGS']['take_profit']
        self.sl = config['TRADE_SETTINGS']['stop_loss']
        self.risk = config['TRADE_SETTINGS']['risk_percentage']
        self.leverage = config['TRADE_SETTINGS']['leverage']
        self.loss_tolerance = config['BOT_SETTINGS']['loss_tolerance']
        self.skip_amount = config['BOT_SETTINGS']['skip_amount']
        self.esl_bool =  config['BOT_SETTINGS']['esl_bool']
        self.fee = config['TRADE_SETTINGS']['fee']
    # ---------------------------------------------------------------------------- #
    #                               BASIC BACKTESTING                              #
    # ---------------------------------------------------------------------------- #

    def no_esl(self, df, indicator_output, tp, sl, margin, leverage):
        # indicator_output = list of either buy,sell or ignore signals.
        # reverse everything so last ticker data becomes first index.
        #tp = tp/10000
        #sl = sl/10000
        # PROBLEMS WITH THIS BACKTESTER:
        # 1. CANT TELL WHETHER HIGH OR LOW WAS REACHED FIRST!!! THEREFORE CANNOT DETERMINE WHETHER TP OR SL WAS ACTIVATED!
        # cum_sl = 

        # First, calculate pips then apply to tp & sl.
        fee = self.fee
        close_data = df['close']
        price = str(close_data.iloc[0])
        num_decimals = price[::-1].find('.')
        tp = tp/(10 ** num_decimals)
        sl = sl/(10 ** num_decimals)
        # -------------------------------------------
        open_data = df['open']
        high_data = df['high']
        low_data = df['low']
        df_times = df.index.to_numpy()
        df_times = pd.Series(df_times).tail(len(indicator_output)).to_list()
        #print(df_times)
        
        close_data = close_data.tail(len(indicator_output)).to_list()
        open_data = open_data.tail(len(indicator_output)).to_list()
        high_data = high_data.tail(len(indicator_output)).to_list()
        low_data = low_data.tail(len(indicator_output)).to_list()

        bought_at = 0
        sold_at = 0
        results = []
        pl_results = []
        current_action = "IGNORE"

        for idx in range(len(indicator_output)-1):
            #starts from oldest data point first 
            if current_action == "BUY":
                price_diff_high = (high_data[idx] - bought_at)
                price_diff_low = (low_data[idx] - bought_at)

                if price_diff_low <= -sl:
                    loss = (low_data[idx] - bought_at)*(bought_amount) - (fee*low_data[idx]*bought_amount)
                    results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                        "bought_at": bought_at, "closed_at": low_data[idx], "PL": loss})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                    bought_at = 0
                    current_action = "IGNORE"

                elif price_diff_high >= tp:
                    profit = (high_data[idx] - bought_at)*(bought_amount) - (fee*high_data[idx]*bought_amount)
                    results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                        "bought_at": bought_at, "closed_at": high_data[idx], "PL": profit})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": profit})
                    bought_at = 0
                    current_action = "IGNORE"
            
                else:
                    pl_results.append({"time_of_close": df_times[idx],  "PL": 0})
                    continue



            elif current_action == "SELL":
                # check sold_at target
                price_diff_high = (sold_at-high_data[idx])
                price_diff_low =  (sold_at-low_data[idx])
                # check stop loss first
                if price_diff_high <= -sl:
                    loss = (sold_at-high_data[idx])*(sold_amount) - (fee*high_data[idx]*sold_amount)
                    results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                        "bought_at": sold_at, "closed_at": high_data[idx], "PL": loss})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                    sold_at = 0
                    current_action = "IGNORE"

                elif price_diff_low >= tp:
                    profit = (sold_at-low_data[idx])*(sold_amount)-(fee*low_data[idx]*sold_amount)
                    results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                        "bought_at": sold_at, "closed_at": low_data[idx], "PL": profit})
                    pl_results.append({"time_of_close": df_times[idx], "PL": profit})
                    sold_at = 0
                    current_action = "IGNORE"
                else:
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    continue
            

            else:
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                if indicator_output[idx] == "BUY":
                    # buy here then skip the current iteration
                    #bought_at = close_data[idx]
                    bought_at = open_data[idx+1]
                    #print('bought_at %s' % bought_at)
                    bought_amount = (margin*leverage)/open_data[idx+1]
                    #print('bought_amount: %s' % bought_amount)
                    current_action = "BUY"
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    continue

                elif indicator_output[idx] == "SELL":
                    
                    # sell here then skip the current iteration
                    #sold_at = close_data[idx]
                    sold_at = open_data[idx+1]
                    #print('sold_at %s' % sold_at)
                    sold_amount = (margin*leverage)/open_data[idx+1]
                    #print('sold_amount: %s' % sold_amount)

                    current_action = "SELL"
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    continue
                else:
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    continue
                
        results_df = pd.DataFrame(results)
        pl_results = pd.DataFrame(pl_results)
        pl_results.set_index('time_of_close', drop=True, inplace=True)
        #print("NO_ESL RESULTS: %s" % results_df)
        return results_df, pl_results


    def with_esl(self, df, indicator_output, tp, sl, margin, leverage, loss_tolerance, skip_amount):
        # indicator_output = list of either buy,sell or ignore signals.
        # reverse everything so last ticker data becomes first index.
        #tp = tp/10000
        #sl = sl/10000
        # First, calculate pips then apply to tp & sl.
        fee = self.fee
        close_data = df['close']
        price = str(close_data.iloc[0])
        num_decimals = price[::-1].find('.')
        tp = tp/(10 ** num_decimals)
        sl = sl/(10 ** num_decimals)
        # -------------------------------------------
        open_data = df['open']
        high_data = df['high']
        low_data = df['low']
        df_times = df.index.to_numpy()
        df_times = pd.Series(df_times).tail(len(indicator_output)).to_list()

        
        close_data = close_data.tail(len(indicator_output)).to_list()
        open_data = open_data.tail(len(indicator_output)).to_list()
        
        bought_at = 0
        sold_at = 0
        results = []
        pl_results = []
        current_action = "IGNORE"
        last_trade_result = "NONE"
        cum_sl_counter = -1
        skip = False
        for idx in range(len(indicator_output)-1):
            #starts from oldest data point first 
            if skip == True:
                skip_counter+=1
                if skip_counter >= skip_amount:
                    skip = False
                    continue
                else:
                    continue
            else:
                if current_action == "BUY":
                    price_diff_high = (high_data[idx] - bought_at)#*bought_amount
                    price_diff_low = (low_data[idx] - bought_at)#*bought_amount
                    # check stop loss first
                    if price_diff_low <= -sl:
                        loss = (low_data[idx] - bought_at)*(bought_amount)-(fee*low_data[idx]*bought_amount)
                        results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                            "bought_at": bought_at, "closed_at": low_data[idx], "PL": loss})
                        pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                        bought_at = 0
                        current_action = "IGNORE"

                        if cum_sl_counter >= loss_tolerance:
                            skip = True
                            skip_counter = 0
                            last_trade_result = "NONE"

                        if last_trade_result == "LOSS":
                            cum_sl_counter+=1
                        else:

                            last_trade_result = "LOSS"
                            cum_sl_counter+=1

                        last_trade_result == "LOSS"
                    elif price_diff_high >= tp:
                        profit = (high_data[idx] - bought_at)*(bought_amount) - (fee*high_data[idx]*bought_amount)
                        results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                            "bought_at": bought_at, "closed_at": high_data[idx], "PL": profit})
                        pl_results.append({"time_of_close": df_times[idx],  "PL": profit})
                        bought_at = 0
                        current_action = "IGNORE"
                        last_trade_result = "PROFIT"
                    else:
                        pl_results.append({"time_of_close": df_times[idx],  "PL": 0})
                        continue



                elif current_action == "SELL":
                    # check sold_at target
                    price_diff_high = (sold_at-high_data[idx])#*sold_amount
                    price_diff_low =  (sold_at-low_data[idx])#*sold_amount

                    # check stop loss first
                    if price_diff_high <= -sl:
                        loss = (sold_at-high_data[idx])*(sold_amount) - (fee*high_data[idx]*sold_amount)
                        results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                            "bought_at": sold_at, "closed_at": high_data[idx], "PL": loss})
                        pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                        sold_at = 0
                        current_action = "IGNORE"

                        if cum_sl_counter >= loss_tolerance:
                            skip = True
                            skip_counter = 0
                            last_trade_result = "NONE"

                        if last_trade_result == "LOSS":
                            cum_sl_counter+=1

                        else:
                            last_trade_result = "LOSS"
                            cum_sl_counter+=1

                        last_trade_result = "LOSS" 

                    elif price_diff_low >= tp:
                        profit = (sold_at-low_data[idx])*(sold_amount)-(fee*low_data[idx]*sold_amount)
                        results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                            "bought_at": sold_at, "closed_at": low_data[idx], "PL": profit})
                        pl_results.append({"time_of_close": df_times[idx], "PL": profit})
                        sold_at = 0
                        current_action = "IGNORE"
                        last_trade_result = "PROFIT"
                    # check stop loss
                    else:
                        pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                        continue
                

                else:
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    if indicator_output[idx] == "BUY":
                        # buy here then skip the current iteration
                        bought_at = open_data[idx+1]
                        
                        bought_amount = (margin*leverage)/open_data[idx+1]

                        current_action = "BUY"
                        pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                        continue

                    elif indicator_output[idx] == "SELL":
                        # sell here then skip the current iteration
                        sold_at = open_data[idx+1]
                        sold_amount = (margin*leverage)/open_data[idx+1]
                        current_action = "SELL"
                        pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                        continue
                    else:
                        pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                        continue
        results_df = pd.DataFrame(results)
        pl_results = pd.DataFrame(pl_results)
        pl_results.set_index('time_of_close', drop=True, inplace=True)
        return results_df, pl_results
    
    def run_batch_backtest(self):

        symbol_list = self.symbol_list
        df_dict = market_data().get_multiple_kline_bars()
        results_dict = {}
        # calculate margin here:
        balance = client_instance().client().Wallet.Wallet_getBalance(coin="USDT").result()[0]
        backtest = backtest_results()
        margin = self.risk * float(balance['result']['USDT']['available_balance'])
        #margin = 1

        if self.esl_bool is True:
            for i in symbol_list:
                # get market_data
                # then indicator strategy
                # then apply backtest
                print("Testing %s with macd strategy using emergency stop loss..." % i)
                output_list = macd_strategy().check_macd_with_stochrsi(df_dict[i])
                results, pl_results = self.with_esl(df_dict[i], output_list, self.tp, self.sl, margin, self.leverage, 2, self.skip_amount)
                #print("results %s" % results)
                tmp = backtest.describe_results(results)
                #print("describe_results: %s" % tmp)
                results_dict[i] = tmp

            return results_dict


        else:
            for i in symbol_list:
                # get market_data
                # then indicator strategy
                # then apply backtest
                print("Testing %s with macd strategy with no esl." % i)
                # for i in strategy_list:
                #print(df_dict[i])
                output_list = macd_strategy().check_macd_with_stochrsi(df_dict[i])
                results, pl_results = self.no_esl(df_dict[i], output_list, self.tp, self.sl, margin, self.leverage)
                #print("results %s" % results)
                tmp = backtest.describe_results(results)
                #print("describe_results: %s" % tmp)
                results_dict[i] = tmp

            return results_dict


    def run_single_backtest(self):
        df = market_data().get_multiple_kline_bars()
        results = []
        # calculate margin here:
        balance = client_instance().client().Wallet.Wallet_getBalance(coin="USDT").result()[0]
        backtest = backtest_results()
        margin = self.risk * float(balance['result']['USDT']['available_balance'])


class backtest_results:
    def __init__(self):
        self.symbol_list = config['BOT_SETTINGS']['symbol_list']
        self.symbol = config['BOT_SETTINGS']['symbol']
        self.tp = config['TRADE_SETTINGS']['take_profit']
        self.sl = config['TRADE_SETTINGS']['stop_loss']
        self.risk = config['TRADE_SETTINGS']['risk_percentage']
        self.leverage = config['TRADE_SETTINGS']['leverage']
        self.loss_tolerance = config['BOT_SETTINGS']['loss_tolerance']
        self.skip_amount = config['BOT_SETTINGS']['skip_amount']
        self.backtest = backtest_program()
        self.batch_backtesting = config['BOT_SETTINGS']['batch_backtesting']

    # need for each symbol -> for each strategy -> save to dictionary -> symbol ->

    # ---------------------------------------------------------------------------- #
    #                        CALCULATE RESULTS FOR STRATEGY                        #
    # ---------------------------------------------------------------------------- #

    def describe_results(self, results):
        # Returns a dictionary of the details of the results
        wl_ratio = 0
        wins = 0
        loss = 0
        average_loss = []
        average_win = []
        max_consecutive_wins = []
        max_consecutive_loss = []
        #print(results)
        for i in results['PL']:
            #print(i)
            if i > 0:
                wins+=1
                average_win.append(i)
            else:
                print(i)
                loss+=1
                average_loss.append(i)
        #print(average_loss)
        #if sum(average_loss) <= 0:
        #    average_loss = [-1]
        #    loss = 1
        profit_factor = abs(sum(average_win)/sum(average_loss))
        #print("profit_factor: %s" % profit_factor)
        max_win = max(average_win)
        max_loss = min(average_loss)
        average_win = sum(average_win)/wins
        average_loss = sum(average_loss)/loss
        total_trades = wins+loss
        win_ratio = wins/(wins+loss)
        loss_ratio = loss/(wins+loss)
        results_details = {'win_rate': win_ratio, 'loss_rate': loss_ratio,
            'average_win': average_win, 'average_loss': average_loss, 
            'total_wins': wins, 'total_loss': loss, 'total_trades': total_trades, 
            'profit_factor': profit_factor, 'total_PL': results['PL'].sum(), 
            'max_win': max_win, 'max_loss': max_loss}
        results_details = dict(results_details)

        return results_details


    # ---------------------------------------------------------------------------- #
    #                        MEASURE POTENTIAL OF A STRATEGY                       #
    # ---------------------------------------------------------------------------- #

    def calculate_profits(self, tp, sl, description, win_error=0.025):
        # tp = take profit in pips
        # sl = stop loss in pips
        # description = dictionary of wins/loss results
        # win_error = max error allowed in win rate to account for real
        # results. must be in decimal.
        profitability_details = []
        win_rate_required = sl/(sl+tp)

        pred_win_rate = description['win_rate']

        if (win_rate_required+win_error) < pred_win_rate:

            print("PROFTIABLE GIVEN THE EXPECTED WIN RATE AND TP/SL SETTINGS!!")
            print("WIN RATE REQUIRED: %s, WIN ERROR ALLOWED: %s" % (win_rate_required, win_error))
            print("EXPECTED WIN RATE: %s" % pred_win_rate)
            print("WIN RATE DIFFERENCE: %s" % (pred_win_rate-win_rate_required))
        else:
            print("NOT PROFITABLE WITH THE EXPECTED WIN RATE AND TP/SL SETTINGS!!")
            print("WIN RATE REQUIRED: %s" % win_rate_required)
            print("EXPECTED WIN RATE: %s" % pred_win_rate)
            print("WIN RATE DIFFERENCE: %s" % (pred_win_rate-win_rate_required))

        return profitability_details


class plotting:
    def __init__(self):
        self.backtest = backtest_program()
        self.symbol_list = config['BOT_SETTINGS']['symbol_list']

    def plot_single(self, pl_results, description, plot_desc):
        # pl_results = dataframe
        # plot_desc = dictionary
        pl_results['cumsum'] = pl_results['PL'].cumsum()
        win_rate = round(description['win_rate']*100, 4)
        num_trades = description['total_trades']
        trace = go.Scatter(x=pl_results.index.to_numpy(), y = pl_results['cumsum'])

        layout = go.Layout(
            title = "%s Profitability, Strategy: %s, Win Rate: %s%" % (plot_desc['symbol'], 
                plot_desc['strategy'], win_rate),
            xaxis = {'title' : 'Time in Minutes, Total Number of Trades: %s' % num_trades},
            yaxis = {'title' : 'Cumulative Profit/Loss (Margin = $%s)' % plot_desc['margin']}
        )
        fig = go.Figure(data = [trace], layout=layout)
        fig.show()
        #raise SystemExit()
    
    def plot_multi(self):
        results_dict = self.backtest.run_batch_backtest()
        symbol_list = self.symbol_list
        
        for i in symbol_list:
            print(results_dict[i])
            # need to plot multiple strategies on the same symbol chart
            # then for each symbol-chart, have a seperate legend showing
            # the win rate of each strategy.
            # 
            



