import toml
import os
from src.market_data.market_data import market_data
from ta.trend import ema_indicator
import datetime as dt

# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #


config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #


class trends:
    def __init__(self):
        self.df = market_data.get_kline_bars()


    def ema_trend(self, window):
        df = self.df
        close_data = df['close']
        ema = ema_indicator(close_data, window=window, fillna=False).dropna()

        results = []

        for i in range(2, len(ema)):
            if ema.iloc[i-2] < ema.iloc[i-1] < ema.iloc[i]:
                results.append("UPTREND")
            elif ema.iloc[i-2] > ema.iloc[i-1] > ema.iloc[i]:
                results.append("DOWNTREND")
            else:
                results.append("SIDEWAYS")

        return results

