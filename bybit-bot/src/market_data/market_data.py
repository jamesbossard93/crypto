from src.client.bybit_client import client_instance
import pytz
import toml
import os
import datetime as dt
import pandas as pd
import time
# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #


config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #

class market_data:
    def __init__(self):
        self.market_type = config['BOT_SETTINGS']['market_type']
        self.symbol = config['BOT_SETTINGS']['symbol']
        self.symbol_list = config['BOT_SETTINGS']['symbol_list']
        self.interval = config['BOT_SETTINGS']['interval']
        self.start_time = config['BOT_SETTINGS']['start_time']


    def get_kline_batch(self, symbol, from_time):

        if config['BOT_SETTINGS']['market_type'] == "spot":
        
            kline = client_instance().client().Kline.Kline_get(symbol=symbol,
                interval=self.interval, **{'from':from_time}).result()[0]

        elif config['BOT_SETTINGS']['market_type'] == "futures":
            kline = client_instance().client().LinearKline.LinearKline_get(symbol=symbol,
                interval=self.interval, **{'from':from_time}).result()[0]

        df = pd.DataFrame(kline['result'])
        # check if null
        if df is None:
            return None
        elif df.empty:
            return None
        df.set_index("start_at", drop=False, inplace=True)
        df.drop(['period', 'interval', 'turnover'], axis=1)
        return df


    def get_kline_bars(self, symbol):
        tz = pytz.timezone("UTC")
        df_list = []
        #start_time = self.start_time
        #start_time = dt.datetime.timestamp(start_time)

        last_datetime = self.start_time #str(int(start_time))

        while True:
            last_datetime = int(dt.datetime.timestamp(last_datetime))
            new_df = self.get_kline_batch(symbol, last_datetime)
            if new_df is None:
                break
            df_list.append(new_df)
            #new_time = dt.datetime.fromtimestamp(max(new_df.index))
            last_datetime = dt.datetime.fromtimestamp(max(new_df.index)) + dt.timedelta(0, 200)
            time.sleep(1)
        
        # Preprocess DataFrame
        df = pd.concat(df_list)
        # convert timestamp to datetime
        tz = pytz.timezone("UTC")
        df_time = df['start_at'].to_numpy()
        dftime = []
        for i in range(len(df_time)):
            dftime.append(dt.datetime.fromtimestamp(df_time[i], tz).strftime("%Y-%m-%d %H:%M"))
        x = pd.Series(dftime)
        df['time'] = x.to_list()
        df.set_index('time', drop=True, inplace=True)
        # filter results
        df = df.drop(['id', 'symbol','period', 'interval', 'start_at', 
            'open_time', 'turnover'], axis=1)
        df = df[['open', 'high', 'low', 'close', 'volume']]

        return df
    
    def get_multiple_kline_bars(self):
        df_dict = {}

        symbol_list = self.symbol_list
        #print("symbol_list: %s" % symbol_list)
        for i in symbol_list:
            print("Acquiring data for %s..." % i)
            df = self.get_kline_bars(i)
            df_dict[i] = df
        
        return df_dict
