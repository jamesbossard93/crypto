.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/bybit_api_bot.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/bybit_api_bot
    .. image:: https://readthedocs.org/projects/bybit_api_bot/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://bybit_api_bot.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/bybit_api_bot/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/bybit_api_bot
    .. image:: https://img.shields.io/pypi/v/bybit_api_bot.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/bybit_api_bot/
    .. image:: https://img.shields.io/conda/vn/conda-forge/bybit_api_bot.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/bybit_api_bot
    .. image:: https://pepy.tech/badge/bybit_api_bot/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/bybit_api_bot
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/bybit_api_bot

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

=============
bybit_api_bot
=============


    A simple ByBit API bot written in python.

This bot runs using the configuration settings located in /config/config.toml file.

To run this trading bot, you need to add your ByBit api key and secret to the config file.

At the moment, this bot can only run on one trading pair per ByBit account. In the future,
it will be able to run on multiple pairs from the same account.


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
