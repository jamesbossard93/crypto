
import os
import toml
from src.client.bybit_client import client_instance
from src.market_data.market_data import market_data
from src.backtest.backtest import backtest_program, backtest_results
from src.backtest.backtest import plotting
from src.trade.trade import trade
from src.strategy.indicator_strategies import macd_strategy
# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #

config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #


if config['BOT_SETTINGS']['backtesting'] is True:
    print('BACKTESTING!')
    print('----------------------------------------------------------------------------')
    if config['BOT_SETTINGS']['batch_backtesting'] is True:
        print("CURRENTLY LOOPING THROUGH 'symbol_list'...")
        #backtest = backtest_program()
        #results = backtest.run_batch_backtest()
        # do plotting here
        #plotting().plot_single()
        #print(results)
        symbol = config['BOT_SETTINGS']['symbol']
        df = market_data().get_kline_bars(symbol)
        print(df)
    else:
        symbol = config['BOT_SETTINGS']['symbol']
        # Plot description
        plot_desc = {}
        plot_desc['symbol'] = symbol
        plot_desc['strategy'] = "check_macd_with_stochrsi"

        tp = config['TRADE_SETTINGS']['take_profit']
        sl = config['TRADE_SETTINGS']['stop_loss']
        risk = config['TRADE_SETTINGS']['risk_percentage']
        leverage = config['TRADE_SETTINGS']['leverage']
        balance = client_instance().client().Wallet.Wallet_getBalance(coin="USDT").result()[0]
        margin = risk * float(balance['result']['USDT']['available_balance'])
        plot_desc['margin'] = str(margin)

        print("RUNNING BACKTEST FOR SYMBOL: %s" % symbol)
        backtest = backtest_program()
        df = market_data().get_kline_bars(symbol)
        indicator = macd_strategy().check_macd_with_stochrsi(df)
        #indicator = macd_strategy().check_macd_with_stochrsiv2(df)
        #results, pl_results = backtest.no_esl(df, indicator, tp, sl, margin, leverage)
        results, pl_results = backtest.with_esl(df, indicator, tp, sl, margin, leverage, 2, 20)
        print(results)
        description = backtest_results().describe_results(results)
        plotting().plot_single(pl_results, description)

else:
    symbol = config['BOT_SETTINGS']['symbol']
    print('RUNNING LIVE BOT!')
    trade().run_trade(symbol)