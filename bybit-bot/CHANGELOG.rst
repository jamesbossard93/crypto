=========
Changelog
=========

Version 0.0.1
===========

- Added macd strategy.
- Included trading fees to backtesting.
- 