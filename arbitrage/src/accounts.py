import json
import time

from binance.client import Client as binance_client
from kucoin.client import Client as kucoin_client


class Accounts:
    def __init__(self, is_sandbox):

        if is_sandbox == True:

            with open("sandbox_config.json", "r") as file:
                config = json.load(file)

        else:

            with open("config.json", "r") as file:
                config = json.load(file)

        self.kucoin_api_key = config['kucoin_api_key']
        self.kucoin_api_secret = config['kucoin_api_secret']
        self.kucoin_api_passphrase = config['kucoin_api_passphrase']
        self.binance_api_key = config['binance_api_key']
        self.binance_api_secret = config['binance_api_secret']

        self.binance_client = binance.client.Client(self.binance_api_key, self.binance_api_secret)
        self.kucoin_client = kucoin.client.Client(self.kucoin_api_key, self.kucoin_api_secret, 
        self.kucoin_api_passphrase)

    def get_clients(self):
        # Returns client objects from list of exchanges.
        