import json
import time
import arbitrage_helper
import binance.client
import kucoin.client
from bittrex_api import BittrexV3

""" with open("config.json", "r") as file:
    config = json.load(file)

kucoin_api_key = config['kucoin_api_key']
kucoin_api_secret = config['kucoin_api_secret']
kucoin_api_passphrase = config['kucoin_api_passphrase']
binance_api_key = config['binance_api_key']
binance_api_secret = config['binance_api_secret']
 """

# NEED TO DO
# 
# - USE OPEN ORDER (BID-ASK) PRICES INSTEAD OF LAST TRADED PRICE
# - ADD LEVERAGE OPTION
# - ADD BUY/SELL FUNCTION
# - ADD SANDBOX FEATURE (is_sandbox for kucoin, testnet for binance)
# - CHECK FUNDS FOR PAPER TRADING
#test = arbitrage_helper.tickers(True)
#print(test)

class arbitrage:

    def __init__(self):
        with open("config.json", "r") as file:
            config = json.load(file)

        self.kucoin_api_key = config['kucoin_api_key']
        self.kucoin_api_secret = config['kucoin_api_secret']
        self.kucoin_api_passphrase = config['kucoin_api_passphrase']
        self.binance_api_key = config['binance_api_key']
        self.binance_api_secret = config['binance_api_secret']
        self.bittrex_api_key = config["bittrex_api_key"]
        self.bittrex_api_secret = config["bittrex_api_secret"]


        self.binance_client = binance.client.Client(self.binance_api_key, 
            self.binance_api_secret)
        
        self.bittrex_client = BittrexV3(api_key=bittrex_api_key, api_secret=bittrex_api_secret,
            max_request_try_count=3, sleep_time=2, debug_level=3, reverse_market_names=True)

        self.kucoin_client = kucoin.client.Client(self.kucoin_api_key, 
            self.kucoin_api_secret, self.kucoin_api_passphrase)

    
    def binance_get_recent_trades(self, ticker_pair):
        trades = self.binance_client.get_recent_trades(symbol=ticker_pair)
        return trades[-1]['price']

    def kucoin_get_ticker_price(self, ticker_pair):

        ticker = self.kucoin_client.get_ticker(symbol=ticker_pair)
        return ticker['price']

    def bittrex_get_ticker_price(self, ticker_pair):
        trades = self.bittrex_client.get_ticker(ticker_pair)
        return trades

    def check_ticker_profitability(self, ticker_pair, min_profit_margin):

        tmp_binance = float(self.binance_get_recent_trades(ticker_pair.replace('-', '')))
        tmp_kucoin = float(self.kucoin_get_ticker_price(ticker_pair))

        binance_diff = tmp_binance - tmp_kucoin
        kucoin_diff = tmp_kucoin - tmp_binance

        if binance_diff > 0:
            if binance_diff - (binance_diff*0.02+binance_diff*min_profit_margin):
                #print("BINANCE PROFIT!")
                #print(binance_diff - (binance_diff*0.02+binance_diff*min_profit_margin))
                return binance_diff - (binance_diff*0.02+binance_diff*min_profit_margin)

            else:

                #print("BINANCE LOSS!")
                #print(binance_diff - (binance_diff*0.02+binance_diff*min_profit_margin))      
                return binance_diff - (binance_diff*0.02+binance_diff*min_profit_margin)          

        elif kucoin_diff > 0:

            if kucoin_diff - (kucoin_diff*0.02+kucoin_diff*min_profit_margin):
                #print("KUCOIN PROFIT!")
                #print(kucoin_diff - (kucoin_diff*0.02+kucoin_diff*min_profit_margin))
                return kucoin_diff - (kucoin_diff*0.02+kucoin_diff*min_profit_margin)

            else:

                #print("KUCOIN LOSS!")
                #print(kucoin_diff - (kucoin_diff*0.02+kucoin_diff*min_profit_margin))
                return kucoin_diff - (kucoin_diff*0.02+kucoin_diff*min_profit_margin)

    def get_USD_profitability(self, ticker_pair, min_profit_margin, exchange, USD_amount):

        profitability = self.check_ticker_profitability(ticker_pair, min_profit_margin)
        profitability = profitability+(profitability*(1+min_profit_margin))
        if exchange == "binance":

            from_end = ticker_pair.find("-")
            from_ticker = ticker_pair[:from_end] + "USDT"
            from_ticker_price = float(self.binance_get_recent_trades(from_ticker))

            return from_ticker_price*profitability*USD_amount

        elif exchange == "kucoin":
            from_end = ticker_pair.find("-")
            from_ticker = ticker_pair[:from_end] + "-USDT"
            from_ticker_price = float(self.kucoin_get_ticker_price(from_ticker))

            return from_ticker_price*profitability*USD_amount

        else:
            print("NO EXCHANGE INPUT!")

        

""" tmp_binance = a.binance_get_recent_trades("BNBBTC")
tmp_kucoin = a.kucoin_get_ticker_price("BNB-BTC")
print(tmp_binance)
print(tmp_kucoin) """

#print(a.get_USD_profitability("BNB-BTC", 0.05, "kucoin", 10000))
profit = 0
for i in range(1000):
    a = arbitrage()

    while 1:
"""         profit = profit + a.get_USD_profitability("BNB-BTC", 0.05, 
                "kucoin", 1000)
        #print(a.get_USD_profitability("BNB-BTC", 0.05, "kucoin", 1000))
        print("net profit: %s" % profit)
 """
        print(type(a.bittrex_get_ticker_price("BTC-XRP")))