>>> loan = client.create_margin_loan(asset='DOGE', amount='200')
>>> loan
{'tranId': 44331254430, 'clientTag': ''}

>>> sell_order = client.create_margin_order(symbol='DOGEUSDT', side='SELL', type='LIMIT', quantity='200', price='0.0566140', timeInForce='GTC')
>>> sell_order
{'symbol': 'DOGEUSDT', 'orderId': 383881033, 'clientOrderId': '4c6vbbea2dApz4DwzJDBNv', 'transactTime': 1615645240787, 'price': '0.056614', 
'origQty': '200', 'executedQty': '200', 'cummulativeQuoteQty': '11.33816', 'status': 'FILLED', 'timeInForce': 'GTC', 'type': 'LIMIT', 'side': 'SELL', 
'fills': [{'price': '0.0566908', 'qty': '200', 'commission': '0.01133816', 'commissionAsset': 'USDT'}], 'isIsolated': False}

>>> buy_order = client.create_margin_order(symbol='DOGEUSDT', side='BUY', type='LIMIT', quantity='200', price='0.0567000', timeInForce='GTC')
>>> buy_order
{'symbol': 'DOGEUSDT', 'orderId': 383886132, 'clientOrderId': 'l57n416pFX3xlOPX5pvJXi', 'transactTime': 1615645446123, 'price': '0.0567', 
'origQty': '200', 'executedQty': '200', 'cummulativeQuoteQty': '11.3269', 'status': 'FILLED', 'timeInForce': 'GTC', 'type': 'LIMIT', 'side': 'BUY', 
'fills': [{'price': '0.0566345', 'qty': '200', 'commission': '0.2', 'commissionAsset': 'DOGE'}], 'isIsolated': False}
>>> repay_order = client.repay_margin_loan(asset='DOGE', amount='200')
>>> repay_order
{'tranId': 44333119415, 'clientTag': ''}
>>> repay_order = client.repay_margin_loan(asset='DOGE', amount='201')
>>> details = client.get_margin_loan_details(asset='DOGE', txId='44331254430')
>>> details
{'total': 1, 'rows': [{'timestamp': 1615644801000, 'txId': 44331254430, 'asset': 'DOGE', 'principal': '200', 'status': 'CONFIRMED', 'clientTag': ''}]}
>>>