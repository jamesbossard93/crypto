from binance.client import Client
import json
import pandas as pd
from math import modf
from decimal import Decimal
import time
from src.ema_scalping import tickers as tks
from binance.enums import *
import re

class binance_trader:

    def __init__(self, is_sandbox):
        self.is_sandbox = is_sandbox
        #self.pip = pips()
        if is_sandbox == True:
            with open("config/sandbox_config.json") as file:
                config = json.load(file)


        else:
            with open("config/config.json") as file:
                config = json.load(file)

        self.binance_api_key = config["binance_api_key"]
        self.binance_api_secret = config["binance_api_secret"]

        self.client = Client(self.binance_api_key,
            self.binance_api_secret)

    def get_balance(self, ticker_code):
        balance = self.client.get_asset_balance(asset=ticker_code)

        return float(balance['free'])

    def quick_buy_ticker(self, ticker_pair, USDT_input, price_target, pip_input, pip_stop_loss):
        # Only supports USDT pairs at the moment!
        # SHORT_SELL is in percentages (decimal) of the amount of total spot
        # balance in USDT to use to buy ticker_pair.
        #balance = self.get_balance("USDT")

        # Round to precision
        precision = tks.tickers("binance", 
            self.is_sandbox).get_ticker_precision(ticker_pair)    
        price_target = round(price_target, precision)
        amount_precision = tks.tickers("binance", 
            self.is_sandbox).get_amount_precision(ticker_pair)  
        ticker_price = tks.tickers("binance", 
            self.is_sandbox).get_ticker_price(ticker_pair)
        ticker_price = round(ticker_price, precision)

        amount = USDT_input/ticker_price
        amount = round(amount, amount_precision)

        # NEED TO ACCOUNT FOR AMOUNT PRECISION
        # Set buy price above 1 pip
        price_target = float(price_target)+pips().get_one_pip(float(price_target))
        price_target = round(price_target, precision)
        #print("AMOUNT = %s" % amount)
        #print("PRICE_TARGET = %s" % price_target)
        order = self.client.order_limit_buy(symbol=ticker_pair, quantity=amount, price=str(price_target))

        # NEED to make stop_loss and TP1 and TP2
        TPT_1 = float(order['price'])+pips().get_one_pip(float(order['price']))*pip_input
        TPT_2 = float(order['price'])+pips().get_one_pip(float(order['price']))*2*pip_input
        stop_loss = float(order['price'])-pips().get_one_pip(float(order['price']))*pip_stop_loss

        # GET UNIX TIMESTAMP IN SECONDS
        UNIX_TIME = time.time()
        return order, TPT_1, TPT_2, stop_loss, "BUY", 0, UNIX_TIME


    def quick_sell_ticker(self, ticker_pair, price_target, amount_to_sell):
        # Only supports USDT pairs at the moment!
        precision = tks.tickers("binance", 
            self.is_sandbox).get_ticker_precision(ticker_pair) 
        price_target = float(price_target)-pips().get_one_pip(float(price_target))
        price_target = round(price_target, precision)
        sell_order = self.client.order_limit_sell(symbol=ticker_pair,
            quantity=amount_to_sell, price=str(price_target))
        
        return sell_order


    def short_sell(self, ticker_pair, USDT_input, price_target, pip_input, pip_stop_loss):
        # Only supports USDT pairs at the moment!
        # CROSS MARGIN ONLY!

        # ticker_pair; e.g. "DOGEUSDT"
        # USDT_input -> amount of USDT to transfer from spot balance.
        # price_target; trigger price for the short sell.

        # STEPS
        # 1. Create margin loan
        # 2. sell loan using 'create_margin_order'
        # 3. return order

        # Round to precision
        precision = tks.tickers("binance", 
            self.is_sandbox).get_ticker_precision(ticker_pair)    
        amount_precision = tks.tickers("binance",
            self.is_sandbox).get_amount_precision(ticker_pair)

        current_price = tks.tickers("binance", 
            self.is_sandbox).get_ticker_price(ticker_pair)
        current_price = round(current_price, precision)
        price_target = round(price_target, precision)
        asset_loan = ticker_pair.split("USDT")[0]
        borrow_amount = USDT_input/current_price
        borrow_amount = round(borrow_amount, amount_precision)

        # Transfer USDT from spot to margin account
        self.client.transfer_spot_to_margin(asset='USDT', 
            amount=str(USDT_input))
        time.sleep(0.1)
        # Create loan for ticker
        self.client.create_margin_loan(asset=asset_loan, 
            amount=str(borrow_amount))
        time.sleep(1)
        # Set sell price one pip below
        price_target = float(price_target)-pips().get_one_pip(float(price_target))
        price_target = round(price_target, precision)
        # Sell loan for USDT
        sell_order = self.client.create_margin_order(symbol=ticker_pair, side=SIDE_SELL, type=ORDER_TYPE_LIMIT, 
            timeInForce=TIME_IN_FORCE_GTC, quantity=borrow_amount, price=str(price_target))
    
        # return sell order with take profit targets and stop loss.
        TPT_1 = float(sell_order['price'])-pips().get_one_pip(float(sell_order['price']))*pip_input
        TPT_1 = round(TPT_1, precision)
        TPT_2 = float(sell_order['price'])-pips().get_one_pip(float(sell_order['price']))*2*pip_input
        TPT_2 = round(TPT_2, precision)
        stop_loss = float(sell_order['price'])+pips().get_one_pip(float(sell_order['price']))*pip_stop_loss
        stop_loss = round(stop_loss, precision)
        UNIX_TIME = time.time()
        return sell_order, TPT_1, TPT_2, stop_loss, "SHORT_SELL", 0, UNIX_TIME


    def repay_short_sell(self, ticker_pair, quantity_input, price_target):
        # Only supports USDT pairs at the moment!
        # CROSS MARGIN ONLY!

        # STEPS
        # 1. Buy back amount using 'create_margin_order' with 'BUY' side
        # 2. Repay the loan using 'repay_order', the amount must also include interest
        # 2.5 Try to use maybe 1 pip as interest.
        ticker_asset = ticker_pair.replace("USDT", "")
        precision = tks.tickers("binance", 
            self.is_sandbox).get_ticker_precision(ticker_pair)
        amount_precision = tks.tickers("binance",
            self.is_sandbox).get_amount_precision(ticker_pair)

        # NEED TO ACCOUNT FOR INTEREST HERE!
        repay_amount = quantity_input*1.005
        price_target = float(price_target) + pips().get_one_pip(float(price_target))
        # Round to precision
        repay_amount = round(repay_amount, amount_precision)
        price_target = round(price_target, precision)


        margin_order = self.client.create_margin_order(symbol=ticker_pair, side="BUY", 
            type="LIMIT", quantity=str(repay_amount), price=str(price_target),
                timeInForce='GTC')
        try:
            while not (margin_order['status'] == 'FILLED'):
                time.sleep(0.1)
        except:
            paid_loan = self.client.repay_margin_loan(asset=ticker_asset,
                amount=str(repay_amount))
        return paid_loan, float(margin_order['cummulativeQuoteQty'])
    


    def track_buy_order(self, order_input, pip_input, pip_stop_loss):
        ticker_pair = order_input['symbol']
        ticker_pair = re.sub(r'\d+', '', ticker_pair)
        ticker_price = tks.tickers("binance", 
            self.is_sandbox).get_ticker_price(ticker_pair)
        target_price = float(order_input['price'])
        TPT_1_input = float(order_input['price']) + pips().get_one_pip(target_price)*pip_input
        TPT_2_input = float(order_input['price']) + pips().get_one_pip(target_price)*2*pip_input
        stop_loss = float(order_input['price']) - pips().get_one_pip(target_price)*pip_stop_loss

        # Round to precision
        precision = tks.tickers("binance", 
            self.is_sandbox).get_ticker_precision(ticker_pair)
        amount_precision = tks.tickers("binance",
            self.is_sandbox).get_amount_precision(ticker_pair)

        sell_amount = float(order_input['executedQty'])*0.5
        sell_amount = round(sell_amount, amount_precision)
        TPT_1_input = round(TPT_1_input, precision)
        TPT_2_input = round(TPT_2_input, precision)    
        stop_loss = round(stop_loss, precision)
        ticker_price = round(ticker_price, precision)

        if ticker_price >= TPT_2_input:

            # Sell for a pip less than current price

            sell_price = ticker_price - pips().get_one_pip(ticker_price)
            sell_price = round(sell_price, precision)
            order = self.quick_sell_ticker(ticker_pair, sell_price, sell_amount)
            UNIX_TIME = time.time()
            return order, "EXIT", UNIX_TIME

        elif ticker_price >= TPT_1_input:

            # Sell for a pip less than current price.
            sell_price = ticker_price - pips().get_one_pip(ticker_price)
            sell_price = round(sell_price, precision)
            order = self.quick_sell_ticker(ticker_pair, sell_price, sell_amount)
            UNIX_TIME = time.time()
            return order, "BUY", UNIX_TIME

        elif ticker_price <= stop_loss:
            sell_amount = float(order_input['executedQty'])
            sell_amount = round(sell_amount, amount_precision)
            
            # Sell for a pip less than current price.
            #stop_loss = ticker_price*0.99
            stop_loss = stop_loss - pips().get_one_pip(ticker_price)
            order = self.quick_sell_ticker(ticker_pair, stop_loss, sell_amount)
            UNIX_TIME = time.time()
            return order, "STOP_LOSS", UNIX_TIME


    def track_short_sell_order(self, order_input, pip_input, pip_stop_loss):
        #
        ticker_pair = order_input['symbol']
        ticker_pair = re.sub(r'\d+', '', ticker_pair)
        ticker_price = tks.tickers("binance", 
            self.is_sandbox).get_ticker_price(ticker_pair)
        target_price = float(order_input['price'])
        TPT_1_input = float(order_input['price']) - pips().get_one_pip(target_price)*pip_input
        TPT_2_input = float(order_input['price']) - pips().get_one_pip(target_price)*2*pip_input
        stop_loss = float(order_input['price']) + ticker_price*pip_stop_loss

        # Round to precision
        precision = tks.tickers("binance", 
            self.is_sandbox).get_ticker_precision(ticker_pair)
        amount_precision = tks.tickers("binance",
            self.is_sandbox).get_amount_precision(ticker_pair)

        sell_amount = float(order_input['executedQty'])*0.5
        sell_amount = round(sell_amount, amount_precision)
        TPT_1_input = round(TPT_1_input, precision)
        TPT_2_input = round(TPT_2_input, precision)    
        stop_loss = round(stop_loss, precision)
        ticker_price = round(ticker_price, precision)

        if ticker_price <= TPT_2_input:
            # Buy for 1 pip higher than current price
            #TPT_2_input = ticker_price*1.01
            # REPAY LOAN
            buy_price = ticker_price + pips().get_one_pip(target_price)
            order = self.repay_short_sell(ticker_pair, 
                float(sell_amount), buy_price)
            
            UNIX_TIME = time.time()
            return order, "EXIT", UNIX_TIME

        elif ticker_price <= TPT_1_input:
            # Buy for 1 pip higher than current price
            #TPT_1_input = ticker_price*1.01
            # REPAY LOAN
            buy_price = ticker_price + pips().get_one_pip(target_price)
            order = self.repay_short_sell(ticker_pair, 
                float(sell_amount), buy_price)

            UNIX_TIME = time.time()
            return order, "SHORT_SELL", UNIX_TIME


        elif stop_loss <= ticker_price:
            sell_amount = float(order_input['executedQty'])
            sell_amount = round(sell_amount, amount_precision)

            # Buy for 1 pip higher than ticker price
            #stop_loss = ticker_price*1.01
            # REPAY LOAN
            buy_price = ticker_price + pips().get_one_pip(target_price)
            order = self.repay_short_sell(ticker_pair, 
                float(sell_amount), buy_price)

            UNIX_TIME = time.time()
            return order, "STOP_LOSS", UNIX_TIME

    def track_orders(self, orders_df, total_profits, pip_input, pip_stop_loss, interval):
        total_profits = 0
        general_orders = pd.DataFrame(columns=["ORDER", "TPT_1", "TPT_2", "STOP_LOSS", "ORDER_TYPE", "PROFIT_LOSS", "UNIX_TIME"])
        interval_dict = {
            "1min": 900,
            "5min": 3600
        }
        
        #orders_df columns=["ORDER", "TPT_1", "TPT_2", "STOP_LOSS", "ORDER_TYPE", "PROFIT_LOSS", "UNIX_TIME"]
        for current_order in orders_df:
            # Gets the ticker symbol from order
            ticker_code = current_order['ORDER'].to_dict()
            ticker_code = ticker_code[next(iter(ticker_code))]

            # ORDER_TYPE = {"BUY", "SHORT_SELL", "STOP_LOSS", "EXIT", "CANCELLED"}
            if current_order['ORDER_TYPE'] == "BUY":
                new_order, STATUS, timestamp = self.track_buy_order(current_order, pip_input, pip_stop_loss)
                if new_order is None:
                    current_time = time.time()
                    time_diff = round(current_time-float(current_order['UNIX_TIME']))
                    if time_diff > interval_dict[interval]:
                        # CANCEL ORDER!
                        cancelled_order = self.cancel_buy_order(current_order)
                        general_orders.loc[ticker_code+str(round(timestamp))] = [cancelled_order, current_order['TPT_1'],
                            current_order['TPT_2'], current_order['STOP_LOSS'], "CANCELLED", 0, timestamp]                            
                else:
                    # calculate profits here
                    profit = float(current_order['ORDER']['cummulativeQuoteQty']) - float(current_order['ORDER']['cummulativeQuoteQty'])   
                    current_order['PROFIT_LOSS'] = profit
                    total_profits += profit
                    # add to general_orders dataframe
                    general_orders.loc[ticker_code+str(round(timestamp))] = [new_order, current_order['TPT_1'], 
                        current_order['TPT_2'], current_order['STOP_LOSS'], STATUS, profit, timestamp]


            elif current_order['ORDER_TYPE'] == "SHORT_SELL":
                new_order, STATUS, timestamp = self.track_short_sell_order(current_order, pip_input, pip_stop_loss)
                if new_order is None:
                    current_time = time.time()
                    time_diff = round(current_time-float(current_order['UNIX_TIME']))
                    if time_diff > interval_dict[interval]:
                        # CANCEL ORDER!
                        cancelled_order = self.cancel_short_sell(current_order)
                        general_orders.loc[ticker_code+str(round(timestamp))] = [cancelled_order, current_order['TPT_1'],
                            current_order['TPT_2'], current_order['STOP_LOSS'], "CANCELLED", 0, timestamp]    
                else:
                    # calculate profits here
                    profit = float(current_order['ORDER']['cummulativeQuoteQty']) - float(current_order['ORDER']['cummulativeQuoteQty'])   
                    current_order['PROFIT_LOSS'] = profit
                    total_profits += profit
                    # add to exit_orders dataframe
                    general_orders.loc[ticker_code+str(round(timestamp))] = [new_order, current_order['TPT_1'], 
                        current_order['TPT_2'], current_order['STOP_LOSS'], STATUS, profit, timestamp]


        return general_orders, total_profits



    
    def cancel_buy_order(self, order_input):
        result = self.client.cancel_order(symbol=order_input['symbol'],
            orderId=order_input['orderId'])
        return result

    def cancel_short_sell(self, order_input):
        result = self.client.cancel_margin_order(symbol=order_input['symbol'],
            orderId=order_input['orderId'])
        return result

class pips:

    def get_one_pip(self, ticker_price):
        # Returns the value of one pip based on ticker price
        # and number of decimals in ticker_price
        d = Decimal(str(ticker_price))
        d = abs(d.as_tuple().exponent)


        if d == 2:
            return (1/100)/ticker_price
        if d == 3:
            return (1/1000)/ticker_price
        if d == 4:
            return (1/10000)/ticker_price
        if d == 5:
            return (1/100000)/ticker_price
        if d == 6:
            return (1/1000000)/ticker_price

    def calculate_pips_profit(self, x, y):
        d = Decimal(str(y))
        d = abs(d.as_tuple().exponent)

        if d == 2:
            multiplier = 0.01
        elif d == 3:
            multiplier = 0.001
        elif d == 4:
            multiplier = 0.0001
        elif d == 5:
            multiplier = 0.00001
        elif d == 6:
            multiplier = 0.000001
        else:
            multiplier = 0.01
        
        pips = round((y-x) / multiplier)

        return int(pips)
