from pathlib import Path
import json
import binance
import pandas as pd
#import tickers
from src.ema_scalping import tickers
from src.ema_scalping import binance_trader


class binance_ema:

    def __init__(self, is_sandbox):
        self.is_sandbox = is_sandbox
        #self.conf_ema_1 = conf_ema_1
        #self.conf_ema_2 = conf_ema_2
        if is_sandbox == True:
            with open("config/sandbox_config.json") as file:
                config = json.load(file)
            self.trade = binance_trader.binance_trader(True)
        else:
            with open("config/config.json") as file:
                config = json.load(file)
            self.trade = binance_trader.binance_trader(False)
        self.binance_api_key = config["binance_api_key"]
        self.binance_api_secret = config["binance_api_secret"]

        self.client = binance.client.Client(self.binance_api_key,
            self.binance_api_secret)
        

    def preprocess_klines(self, klines_data):
        #returns OHLCV in pandas dataframe
        tmp = []
        for i in klines_data:
            tmp.append([float(i[1]), float(i[2]),
                float(i[3]), float(i[4]), float(i[5])])
        tmp = pd.DataFrame(tmp, columns=['open', 'high', 'low', 'close',
            'volume'])
        return tmp

    def get_current_price(self, ticker_pair):
        tmp = self.client.get_recent_trades(symbol=ticker_pair)
        return float(tmp[-1:]['price'])

    def hour_data(self, ticker_pair):
        # Anchor chart
        candles = self.client.get_klines(symbol=ticker_pair, 
            interval=self.client.KLINE_INTERVAL_1HOUR)

        tmp = self.preprocess_klines(candles)
        #tmp = pd.DataFrame(tmp, columns=['open', 'high', 'low', 'close',
        #    'volume'])

        return tmp

    def halfhour_data(self, ticker_pair):
        # Anchor chart
        candles = self.client.get_klines(symbol=ticker_pair, 
            interval=self.client.KLINE_INTERVAL_30MINUTE)
        
        tmp = self.preprocess_klines(candles)
        #mp = pd.DataFrame(tmp, columns=['open', 'high', 'low', 'close',
        #    'volume'])
        return tmp


    def five_min_data(self, ticker_pair):
        # For 8 & 21 EMA
        candles = self.client.get_klines(symbol=ticker_pair, 
            interval=self.client.KLINE_INTERVAL_5MINUTE)
        
        tmp = self.preprocess_klines(candles)
        #tmp = pd.DataFrame(tmp, columns=['open', 'high', 'low', 'close',
        #    'volume'])
        return tmp

    def get_five_min_trigger(self, ticker_pair, trade_type, five_min_range):
        # Gets the entry point if buy or sell signal is activated from hourly.
        data = self.five_min_data(ticker_pair)
        ema_8 = self.get_ema(8, data)
        ema_13 = self.get_ema(13, data)
        ema_21 = self.get_ema(21, data)
        current_price = float(tickers.tickers("binance", 
            self.is_sandbox).get_ticker_price(ticker_pair))
        if trade_type == "BUY":
            # Check for ema crossing...
            for n in range(five_min_range, -1, -1):
                if ema_21[-n] < ema_13[-n] < ema_8[-n]:
                    continue
                else:
                    return "NO 5MIN ENTRY POINT"

            # Searching for trigger bar...
            for n in range(five_min_range, -1, -1):
            
                if  ema_8[-n] < data['low'][-n]:
                    continue
                else:
                    # Count back 5 candles and pick highest price
                    # for trigger price
                    y = 5
                    tmp_candles = []
                    for y in range(4,-1,-1):
                        tmp_candles.append(float(data['high'][-y]))
                        print("SEARCHING FOR TRIGGER PRICE: %s" % float(data['high'][-y]))
                    target_price = max(tmp_candles)
                    # check if highest price is greater than current price
                    if current_price < target_price:
                        #return target_price
                        print("target price is: %s" % target_price)
                        precision = tickers.tickers('binance', self.is_sandbox).get_ticker_precision(ticker_pair)
                        target_price = round(target_price, precision)
                        return target_price

                    else:
                        # Target price is lower than current price.
                        target_price = current_price+self.trade.pips().get_one_pip(current_price)*2
                        #target_price = current_price*1.01
                        print("TARGET PRICE IS TOO LOW FOR 5MIN ENTRY")
                        print("SETTING TARGET PRICE TO GREATER THAN 1 PIP OF CURRENT PRICE")
                        precision = tickers.tickers('binance', self.is_sandbox).get_ticker_precision(ticker_pair)
                        target_price = round(target_price, precision)
                        return target_price
            # NO TRIGGGER BAR FOUND!     
            print("NO TRIGGER BAR FOUND!")
            print("WILL USE CURRENT PRICE PLUS PIP")
            target_price = current_price+self.trade.pips().get_one_pip(current_price)*2
            #target_price = current_price*1.01
            print("TARGET PRICE IS TOO LOW FOR 5MIN ENTRY")
            print("SETTING TARGET PRICE TO GREATER THAN 1 PIP OF CURRENT PRICE")
            precision = tickers.tickers('binance', self.is_sandbox).get_ticker_precision(ticker_pair)
            target_price = round(target_price, precision)
            return target_price



        elif trade_type == "SELL":
            # Check for ema crossing...
            for n in range(five_min_range, -1, -1):
                if ema_21[-n] > ema_13[-n] > ema_8[-n]:
                    continue
                else:
                    return "NO 5MIN ENTRY POINT"

            # Searching for trigger bar...
            for n in range(five_min_range, -1, -1):
            
                if  ema_8[-n] > data['low'][-n]:
                    continue
                else:
                    # Count back 5 candles and pick highest price
                    # for trigger price
                    y = 5
                    tmp_candles = []
                    for y in range(4,-1,-1):
                        tmp_candles.append(float(data['high'][-y]))
                        print("SEARCHING FOR TRIGGER PRICE: %s" % float(data['high'][-y]))
                    target_price = max(tmp_candles)
                    # check if highest price is greater than current price
                    if current_price < target_price:
                        #return target_price
                        print("target price is: %s" % target_price)
                        precision = tickers.tickers('binance', self.is_sandbox).get_ticker_precision(ticker_pair)
                        target_price = round(target_price, precision)
                        return target_price

                    else:
                        # Target price is lower than current price.
                        target_price = current_price+self.trade.pips().get_one_pip(current_price)*2
                        #target_price = current_price*1.01
                        print("TARGET PRICE IS TOO LOW FOR 5MIN ENTRY")
                        print("SETTING TARGET PRICE TO GREATER THAN 1 PIP OF CURRENT PRICE")
                        precision = tickers.tickers('binance', self.is_sandbox).get_ticker_precision(ticker_pair)
                        target_price = round(target_price, precision)
                        return target_price
            # NO TRIGGGER BAR FOUND!     
            print("NO TRIGGER BAR FOUND!")
            print("WILL USE CURRENT PRICE PLUS PIP")
            target_price = current_price+self.trade.pips().get_one_pip(current_price)*2
            #target_price = current_price*1.01
            print("TARGET PRICE IS TOO LOW FOR 5MIN ENTRY")
            print("SETTING TARGET PRICE TO GREATER THAN 1 PIP OF CURRENT PRICE")
            precision = tickers.tickers('binance', self.is_sandbox).get_ticker_precision(ticker_pair)
            target_price = round(target_price, precision)
            return target_price



    def one_min_data(self, ticker_pair):
        # Need to figure out 
        candles = self.client.get_klines(symbol=ticker_pair, 
            interval=self.client.KLINE_INTERVAL_5MINUTE)
        
        tmp = self.preprocess_klines(candles)
        #tmp = pd.DataFrame(tmp, columns=['open', 'high', 'low', 'close',
        #    'volume'])
        return tmp


    def get_ema(self, ema_num, ticker_data):
        # Returns list of exponential moving average.
        #tmp = pd.DataFrame(ticker_data, columns=['open', 'high', 'low', 'close',
        #    'volume'])
        ticker_data = ticker_data['close'].ewm(span=ema_num, adjust=False).mean()
        ticker_data = ticker_data.tolist()
        
        return ticker_data


    def check_hourly_buy_signal(self, ticker_data, ema_8, ema_21, hourly_range):

        for i in range((len(ticker_data)-hourly_range), len(ticker_data)):
            print("Current confirmation loop is %s" % i)
            if ema_21[i] < ema_8[i] < ticker_data['low'][i]:
                continue
            else:
                return "NO BUY"
        
        return "BUY"


    def check_hourly_sell_signal(self, ticker_data, ema_8, ema_21, hourly_range, ticker_pair):

        # Check if cross margin is available
        try:
            self.client.get_margin_symbol(symbol=ticker_pair)
        except:
            return "NO SELL"

        for i in range((len(ticker_data)-hourly_range), len(ticker_data)):
            # print("ticker_data high: %s" % ticker_data['high'][i])
            #price_diff = ema_21[i]-ema_8[i]-ticker_data["high"][i]
            #print("hourly sell diff is %s" % price_diff)
            #print("EMA_21 = %s" % ema_21[i])
            #print("EMA_8 = %s" % ema_8[i])
            #print("EMA_21 = %s" % ema_21[i])
            #is_valid_margin = bool()
            # NEED TO ADD MARGIN CHECKER HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            #ema_equality = ema_8[i] < ema_21[i]
            if ticker_data['high'][i] < ema_8[i] < ema_21[i]:
                continue
            else:
                return "NO SELL"
        return "SELL"


    def check_hourly_confirmation(self, ticker_pair, hourly_range):
        ticker_data = self.hour_data(ticker_pair)
        conf_ema_1 = self.get_ema(8, ticker_data)
        conf_ema_2 = self.get_ema(21, ticker_data)

        if self.check_hourly_buy_signal(ticker_data, conf_ema_1, 
            conf_ema_2, hourly_range) == "BUY":

            return "BUY"

        elif self.check_hourly_sell_signal(ticker_data, conf_ema_1,
            conf_ema_2, hourly_range, ticker_pair) == "SELL":

            return "SELL"

        else:

            return "NO POSSIBLE TRADES FOR %s" % ticker_pair