import sys



class input:
    def __init__(self, inputs_list, num_inputs, checker_list):
        self.inputs_list = inputs_list
        self.num_inputs = num_inputs
        self.checker_list = checker_list

    def verify_inputs(self):
        num_inputs = len(self.inputs_list)
        #Example input: python ema_scalping.py binance real 5min 5 3 100 250
        if len(self.inputs_list) == len(self.checker_list):
            for i in range(num_inputs):
                
                if self.checker_list[i] == int:
                    if isinstance(int(self.inputs_list[i]), int):
                        continue
                    else:
                        output_string = "Invalid input argument type: %s, requires of type int." % self.inputs_list[i]
                        return False, output_string
                elif self.inputs_list[i] in self.checker_list[i]:
                    continue
                else:
                    output_string = "Invalid input argument %s, accepted inputs are %s" % (self.inputs_list[i], self.checker_list[i])
                    return False, output_string
                
        else:
            return False, "Invalid Number of arguments, please refer to README file."

        return True, "True"