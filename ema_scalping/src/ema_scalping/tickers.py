import binance
import json
import decimal


class tickers(object):
    # currently only supports binance
    def __init__(self, exchange_input, is_sandbox):
        self.exchange = exchange_input
        self.is_sandbox = is_sandbox
        if self.is_sandbox == True:
            with open("config/sandbox_config.json") as file:
                config = json.load(file)
        else:
            with open("config/config.json") as file:
                config = json.load(file)            
        self.binance_api_key = config["binance_api_key"]
        self.binance_api_secret = config["binance_api_secret"]

        self.client = binance.client.Client(self.binance_api_key,
            self.binance_api_secret)
        
    def get_all_USDT_tickers(self, exchange):

        if self.exchange == "binance":
            usdt_list = []
            prices = self.client.get_all_tickers()

            for i in prices:
                if i['symbol'][-4:] == "USDT":
                    usdt_list.append(i['symbol'])

            return usdt_list
    
    def get_USDT_tickers_with_filter(self, ticker_list):

        if self.exchange == "binance":
            usdt_list = []
            prices = self.client.get_all_tickers()

            for i in prices:
                if (i['symbol'][-4:] == "USDT") and (i['symbol'][-4:] not in ticker_list):
                    usdt_list.append(i['symbol'])

            return usdt_list


    def get_all_USDT_prices(self, exchange):

        if self.exchange == "binance":
            usdt_prices = []
            prices = self.client.get_all_tickers()

            for i in prices:
                if i['symbol'][-4:] == "USDT":
                    usdt_prices.append(float(i['price']))

            return usdt_prices

    def get_ticker_price(self, ticker_pair):

        if self.exchange == "binance":
        
            prices = self.client.get_all_tickers()

            for i in prices:
                if i['symbol'] == ticker_pair:
                    return float(i['price'])
                

        

    def get_ticker_precision(self, ticker_pair):
        # Returns the minimum number of decimal places used for orders.
        ticker = self.client.get_symbol_info(ticker_pair)
        precision = float(ticker['filters'][0]['minPrice'])
        d = decimal.Decimal(str(precision))
        d = abs(d.as_tuple().exponent)
        return d

    def get_amount_precision(self, ticker_pair):
        # Returns the minimum number of decimal places used for orders.
        ticker = self.client.get_symbol_info(ticker_pair)
        precision = float(ticker['filters'][2]['minQty'])
        d = decimal.Decimal(str(precision))
        d = abs(d.as_tuple().exponent)
        return d

    #def get_all_tickers(self):


    #def get_all_USDT_prices(self):

    #def get_all_current_prices(self):
