import pandas as pd
import binance
from binance.enums import *
import sys
import time
import datetime
from src.ema_scalping import binance_ema
from src.ema_scalping import binance_trader
from src.ema_scalping import tickers
import re
from src.ema_scalping import inputs


def main():
    # Example input: python ema_scalping.py binance real 5min 10 3 20 800
    # where sys.argv[1] == exchange
    # sys.argv[2] == real trading or paper trading
    # sys.argv[3] == trading interval (e.g. 1min, 5min etc..)
    # sys.argv[4] == number of pips for take profit target
    # sys.argv[5] == number of pips for stop loss
    # sys.argv[6] == amount of USDT per trade
    # sys.argv[7] == minimum total collateral USDT
    # Create checker list for input arguments
    checker = [['binance', 'kucoin'], ['real', 'paper'], 
        ['1min', '5min', '15min'], int, int, int, int]
    
    # Initialize inputs class, skip first argument, verifies input
    arguments = inputs.input(sys.argv[1:], len(sys.argv[1:]), checker)

    # dataframe of all current orders, either bought or short sold
    # columns = ["ORDER", "TPT_1", "TPT_2", "STOP_LOSS", "ORDER_TYPE", "PROFIT_LOSS"]
    # ORDER_TYPE = {"BUY", "SHORT_SELL", "STOP_LOSS", "EXIT", "CANCELLED"}
    # MAY NEED TIMESTAMP FOR TRACKED ORDERS (IF TDIFF > 10mins, cancel order)
    tracked_orders = pd.DataFrame(columns=["ORDER", "TPT_1", "TPT_2", "STOP_LOSS", "ORDER_TYPE", "PROFIT_LOSS", "UNIX_TIME"])
    # This loop will continue until user cancels with Ctrl-C.
    # TO DO:
    # - other exchanges; kucoin, bittrex,
    # short sell
    #
    #
    # First, verify input arguments
    verify_bool, err_msg = arguments.verify_inputs()
    if verify_bool == True:
        total_profits = 0
        if sys.argv[1] == "binance":
            # Set credentials to binance.
            trade = binance_trader.binance_trader(False)
        try:
            while True:

                if sys.argv[1] == "binance":
                    trade_balance = trade.get_balance("USDT")
                    if trade_balance > float(sys.argv[7]):
                        #ema = binance_ema.binance_ema()
                        #trade = binance_trader.binance_trader()
                        if sys.argv[2] == "real":
                            ema = binance_ema.binance_ema(False)
                            trade = binance_trader.binance_trader(False)
                            print("USING REAL ACCOUNT")
                            trade_balance = trade.get_balance("USDT")

                            if sys.argv[3] == "1min":
                                print("TO DO")
                                break
                            elif sys.argv[3] == "5min":


                                # First get all tickers with the tickers module. if tracked_orders is not empty,
                                # then use ticker index as filter.
                                tks = tickers.tickers("binance", True)
                                if len(tracked_orders.index) == 0:
                                    all_tickers = tks.get_all_USDT_tickers("binance")
                                else:
                                    ticker_list = tracked_orders.index.tolist()
                                    for ticker in ticker_list:
                                        ticker = re.sub(r'\d+', '', ticker)
                                    all_tickers = tks.get_USDT_tickers_with_filter(ticker_list)


                                # Secondly, cycle through all USDT tickers to find a list of possible targets.
                                # Bot will filter between potential buys or short sells then execute orders.
                                for i in all_tickers:
                                    print("CURRENT TRADING SYMBOL: %s" % i)

                                    # Check if maximum order spending is reached.
                                    if trade_balance <= float(sys.argv[7]):
                                        print("ORDER SPENDING LM")
                                        time.sleep(10)
                                        continue


                                    elif ema.check_hourly_confirmation(i, 5) == "BUY":
                                        print("HOURLY BUY SIGNAL TRIGGERED")
                                        trigger_price = ema.get_five_min_trigger(i, "BUY", 8)
                                        print("TARGET BUY PRICE AT %s" % trigger_price)
                                        if type(trigger_price) == str:
                                            print("NO 5MIN ENTRY POINT")
                                            continue
                                        else:
                                            print("BUYING TICKER")
                                            order, TPT_1, TPT_2, stop_loss, ORDER_TYPE, PROFIT_LOSS, UNIX_TIME  = trade.quick_buy_ticker(i, 
                                            float(sys.argv[6]), trigger_price, int(sys.argv[4]), int(sys.argv[5]))
                                            
                                            # ADD BUY ORDER TO "tracked_orders" DataFrame
                                            tracked_orders.loc[i+str(round(UNIX_TIME))] = [order, TPT_1, TPT_2, stop_loss, ORDER_TYPE, 
                                                PROFIT_LOSS, UNIX_TIME]
                                            trade_balance-=float(sys.argv[6])
                                            #print(bought)
                                            continue


                                    elif ema.check_hourly_confirmation(i, 5) == "SELL":
                                        print("HOURLY SHORT SELL SIGNAL TRIGGERED")
                                        trigger_price = ema.get_five_min_trigger(i, "SELL", 8)
                                        print("TARGET SELL PRICE AT %s" % trigger_price)
                                        if type(trigger_price) == str:
                                            print("NO ENTRY POINT")
                                            continue
                                        else:
                                            print("SHORT SELLING TICKER")
                                            order, TPT_1, TPT_2, stop_loss, ORDER_TYPE, PROFIT_LOSS, UNIX_TIME = trade.short_sell(i, float(sys.argv[6]), 
                                                trigger_price, int(sys.argv[4]), int(sys.argv[5]))
                                            tracked_orders.loc[i+str(round(UNIX_TIME))] = [order, TPT_1, TPT_2, stop_loss, 
                                                ORDER_TYPE, PROFIT_LOSS, UNIX_TIME]
                                            trade_balance-=float(sys.argv[6])
                                            continue
                                # DO A WHILE LOOK HERE OVER ALL ORDERS, FILTER OUT ORDER TICKERS THEN MOVE
                                # BACK TO FOR I IN ALL_TICKERS LOOP, IF NO BUY OR SELL SIGNAL, WAIT IN WHILE LOOP.
                                # THEN TRY AGAIN.
                                #try:
                                #    while True:
                                #            do_something()
                                #except KeyboardInterrupt:
                                #    pass
                                if len(tracked_orders) < 1:
                                    continue
                                else:
                                    tracked_orders, total_profits = trade.track_orders(tracked_orders, total_profits, int(sys.argv[4]), int(sys.argv[5]), "5min")
                

                elif sys.argv[1] == "kucoin":
                    print("NEED TO DO KUCOIN")

                else:
                    print("Insufficient spot balance, need more than $%s USDT." % str(sys.argv[7]))
                    exit()


        except KeyboardInterrupt:
            print("EMA scalping bot ended with key Ctrl-C")
            print("creating csv of orders")
            current_datetime = time.strftime('%d%m%y_%H%M')
            filename = current_datetime+str("orders")
            # NEED TO CREATE FILE OF INPUTS
            tracked_orders.to_csv(r'data\%s.csv'% filename)
            #time.sleep(1)
            
            print("Orders saved!")
            print("Final Profit = %s" % total_profits)
            exit()
        

    else:
        print(err_msg)
        exit()

















if __name__ == '__main__':
  main()