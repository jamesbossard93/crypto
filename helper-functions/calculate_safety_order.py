
# Inputs
side = input("Enter your position side (buy/sell): ")

entry = float(input("Enter your entry point: "))

lot_size = int(input("What is the volume (lot size) traded? "))

balance = float(input("What is your total account balance? "))

max_dd = float(input("What is your maximum acceptable drawdown in percentage (e.g. 0.05 for 5% )? "))

num_safety_orders = int(input("What is the number of safety orders? "))

# Maximum drawdown in currency
max_dd_value = balance*max_dd

# Get precision
f = str(entry)

num_decimal = f[::-1].find('.')


# Calculates the long safety orders
if side == "buy":
     
    if num_safety_orders < 2:

        stop_loss = entry - max_dd_value/(lot_size*(num_safety_orders+1))
        print(stop_loss)
        price = entry - (entry - stop_loss)/2
        price = round(price, num_decimal)
        stop_loss = round(stop_loss, num_decimal)
        print("The safety order price is: %s" % price)
        print("The stop loss is at: %s: " % stop_loss)
        quit()
    else:

        stop_loss = entry - max_dd_value/(lot_size*(num_safety_orders+1))
 
        price_interval = (entry - stop_loss)/num_safety_orders
    
        # Shifts the safety orders by one price interval to account for stop loss.
        last_so_price = stop_loss + price_interval

        price_interval = (entry - last_so_price)/num_safety_orders

        so_list = []
        so = entry - price_interval
        so_list.append(round(so, num_decimal))
        for i in range(num_safety_orders-1): 
            so -= price_interval
            so_list.append(round(so, num_decimal))
        
        print("The list of safety order prices are: %s" % so_list)
        print("The stop loss is at: %s" % stop_loss)
        quit()

# Calculates the short safety orders
if side == "sell":
    
    if num_safety_orders < 2:
        stop_loss = entry + max_dd_value/(lot_size*(num_safety_orders+1))
        price = entry + (stop_loss - entry)/2
        price = round(price, num_decimal)
        stop_loss = round(stop_loss, num_decimal)
        print("The price of the safety order is: %s" % price)
        print("The stop loss is at: %s" % stop_loss)
        quit()
    else:
        last_so_price = entry + max_dd_value/(lot_size*(num_safety_orders+1))

        price_interval = (last_so_price - entry)/num_safety_orders
    
        # Shifts the safety orders by one price interval to account for drawdown.
        last_so_price = last_so_price - price_interval

        price_interval = (last_so_price - entry)/num_safety_orders
    
        so_list = []
 
        so = entry + price_interval
        so_list.append(round(so, num_decimal))
        for i in range(num_safety_orders-1): 
            so += price_interval 

            so_list.append(round(so, num_decimal))

        print("The list of safety order prices are: %s" % so_list)
        quit()

