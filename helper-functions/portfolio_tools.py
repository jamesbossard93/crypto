import pandas as pd
import numpy as np







def omega_ratio(portfolio: pd.DataFrame, target: float==0.1, weights: list()):
	""" Calculates the ratio of gains vs losses weighted 
	by probability  of a group of returns (stocks, cryptos etc...) 
	for some threshold return target. """
	# portfolio = dataframe of returns for selected stocks, cryptos etc...
	# target = annualized target return for portfolio in decimal (10% == 0.1)
	# weights = the weight or investment of each individual stock, can be used to
	# optimize portfolio.
	avg_returns = portfolio.mean().tolist()
	if not weights:
		# distribute the weights evenly between the stocks
		num_cols = int(portfolio.shape[1])
		weights = [1/num_cols for i in range(num_cols)]

	avg_daily_return = sum(a*b for a, b in zip(avg_returns, weights))
	target_daily_return = (1+target)^(1/len(portfolio.index)) - 1
	numerator = avg_daily_return - target_daily_return
	portfolio['weighted_returns'] = np.sum((portfolio.values*weights), axis=1)
	portfolio['downside'] = [max(i-target_daily_return,0) for i in portfolio['weighted_returns']]
	denominator = portfolio['downside'].mean()

	omega = (numerator/denominator)+1

	return omega
