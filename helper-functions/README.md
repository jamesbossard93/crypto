# Helper Functions

## Here contains a list of scripts to help traders with risk management, profitability etc...






## List of Helper Functions:

### - Safety Order Calculator (calculate_safety_order.py)
    - Calculates safety order given; position side, entry price, 
        lot size, maximum acceptable drawdown, balance and number of safety orders.
    - Calculator does NOT take into account spread


